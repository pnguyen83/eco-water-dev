@isTest
global class skedHttpCalloutMock implements HttpCalloutMock {
    public String jsonRespond;
    public skedHttpCalloutMock(String data){
        this.jsonRespond   = data;
    }

    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();

        res.setHeader('Content-Type', 'application/json');
        res.setBody(this.jsonRespond);
        res.setStatusCode(200);
        System.debug('call response');
        return res;
    }
}