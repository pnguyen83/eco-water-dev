global class skedCommonService {
    public static List<skedEwtModels.Option> getPickListValues(string objectApiName, string fieldApiName) {
        List<string> picklistValues = new List<string>();

        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectApiName);
        DescribeSObjectResult objDescribe = targetType.getDescribe();
        map<String, SObjectField> mapFields = objDescribe.fields.getmap();
        SObjectField fieldType = mapFields.get(fieldApiName);
        DescribeFieldResult fieldResult = fieldType.getDescribe();

        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple) {
            picklistValues.add(f.getValue());
        }
        picklistValues.sort();

        List<skedEwtModels.Option> result = new List<skedEwtModels.Option>();
        for (string picklistValue : picklistValues) {
            result.add(new skedEwtModels.Option(picklistValue, picklistValue));
        }
        return result;
    }

    public static String getResourceCategoryFromUserId() {
    	String userId = UserInfo.getUserId();

    	List<sked__Resource__c> skedReses = [SELECT id, sked__User__c, sked__Category__c
    											FROM sked__Resource__c
    											WHERE sked__User__c = :userId
    											AND sked__Is_Active__c = true];

    	if (skedReses == null || skedReses.isEmpty()) {
    		throw new skedException(skedConstants.RESOURCE_DOES_NOT_HAVE_USER);
    	}

    	return skedReses.get(0).sked__Category__c;
    }

    public static List<sked__Slot__c> getSlotInformation(skedEwtModels.SlotFilter filter) {
        List<sked__Slot__c> skedSlots = new List<sked__Slot__c>();
        String query = 'SELECT Id, Name, sked__Customer_White_Listed__c, sked_Day__c, sked__Job__c,';
        query += 'sked_Job_Type__c, sked__Location_White_Listed__c, sked__Quantity__c,';
        query += 'sked__Resource_Type__c, sked_Slot_Active_Start_Date__c, sked_Slot_Active_End_Date__c,';
        query += 'sked_Slot_Start_Time__c, sked_Slot_End_Time__c';

        if (filter.isConfigData == false) {
            query += ', (SELECT id, Name, sked__Start__c, sked__Finish__c FROM sked_Jobs__r';
            query += ' WHERE sked__Job_Status__c != \'' + skedConstants.JOB_STATUS_CANCELLED + '\'';
            query += ' AND sked__start__c <= ' + filter.endTime + ' AND sked__finish__c >= ' + filter.startTime + ') ';
            query += ' FROM sked__Slot__c';
            query += ' WHERE sked_Slot_Active_Start_Date__c <= ' + filter.endDate + ' AND sked_Slot_Active_End_Date__c >= ' + filter.startDate;
            query += ' AND sked_Job_Type__c = \'' + filter.jobType + '\'';
        }
        else {
            query += ' FROM sked__Slot__c';
            //query += ' WHERE sked_Job_Type__c = \'' + filter.jobType + '\'';
        }

        System.debug('query ' + query);
        skedSlots = (List<sked__Slot__c>)Database.query(query);

        return skedSlots;
    }


    public static Map<String, List<String>> getDependentOptionsImpl(String objectApiName, String ctrlFieldName, String dependentField) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectApiName);
        DescribeSObjectResult objDescribe = targetType.getDescribe();
        map<String, SObjectField> mapFields = objDescribe.fields.getmap();
        Schema.SObjectField theField = mapFields.get(dependentField);
        Schema.SObjectField ctrlField = mapFields.get(ctrlFieldName);
        // validFor property cannot be accessed via a method or a property,
        // so we need to serialize the PicklistEntry object and then deserialize into a wrapper.
        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<skedEwtModels.PicklistEntryWrapper> depEntries =
                wrapPicklistEntries(theField.getDescribe().getPicklistValues());

        // Set up the return container - Map<ControllingValue, List<DependentValues>>
        Map<String, List<String>> objResults = new Map<String, List<String>>();
        List<String> controllingValues = new List<String>();

        for (Schema.PicklistEntry ple : contrEntries) {
            String label = ple.getLabel();
            objResults.put(label, new List<String>());
            controllingValues.add(label);
        }

        for (skedEwtModels.PicklistEntryWrapper plew : depEntries) {
            String label = plew.label;
            String validForBits = base64ToBits(plew.validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                // For each bit, in order: if it's a 1, add this label to the dependent list for the corresponding controlling value
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                    objResults.get(controllingValues.get(i)).add(label);
                }
            }
        }

        return objResults;
    }

    public static List<skedModels.product> getProducts() {
        List<skedModels.product> productModels = new List<skedModels.product>();
        for (Product2 prod : [ SELECT Id, Name, Family, sked_Product_Job_Type__c, IsActive, Sequence__c
                                FROM Product2
                                WHERE IsActive = true
                                ORDER BY Sequence__c ASC
                                LIMIT 300]) {
            productModels.add(new skedModels.product(prod));
        }
        return productModels;
    }
    public static List<skedModels.region> getRegions() {
        List<skedModels.region> regionModels = new List<skedModels.region>();
        for (sked__Region__c reg : [ SELECT Id, Name, sked__Timezone__c
                                    FROM sked__Region__c
                                    ORDER BY Name ASC
                                    LIMIT 100 ]) {
            regionModels.add(new skedModels.region(reg));
        }
        return regionModels;
    }
    public static List<skedEwtModels.Option> getCheckBoxLabel() {
        List<skedEwtModels.Option> checkBoxLabels =  new List<skedEwtModels.Option>();
        checkBoxLabels.add(new skedEwtModels.Option('siteSurvey', sked__Job__c.sked_Site_Survey_Required__c.getDescribe().getLabel()));
        checkBoxLabels.add(new skedEwtModels.Option('digTess', sked__Job__c.sked_Dig_Tess_Required__c.getDescribe().getLabel()));
        checkBoxLabels.add(new skedEwtModels.Option('permit', sked__Job__c.sked_Permit__c.getDescribe().getLabel()));
        checkBoxLabels.add(new skedEwtModels.Option('garageManifold', sked__Job__c.sked_Garage_Manifold__c.getDescribe().getLabel()));
        checkBoxLabels.add(new skedEwtModels.Option('exteriorManifold', sked__Job__c.sked_Exterior_Manifold__c.getDescribe().getLabel()));
        checkBoxLabels.add(new skedEwtModels.Option('main', sked__Job__c.sked_1_25_Main__c.getDescribe().getLabel()));
        checkBoxLabels.add(new skedEwtModels.Option('trench', sked__Job__c.sked_40_ft_Trench__c.getDescribe().getLabel()));
        checkBoxLabels.add(new skedEwtModels.Option('shedExteriorPlacement', sked__Job__c.sked_Shed_Exterior_Placement__c.getDescribe().getLabel()));
        checkBoxLabels.add(new skedEwtModels.Option('driveBore', sked__Job__c.sked_Driveway_Bore__c.getDescribe().getLabel()));
        checkBoxLabels.add(new skedEwtModels.Option('iceLine', sked__Job__c.sked_Ice_Line__c.getDescribe().getLabel()));
        checkBoxLabels.add(new skedEwtModels.Option('installSpecialAttribute1', sked__Job__c.sked_InstallSpecialAttribute1__c.getDescribe().getLabel()));
        checkBoxLabels.add(new skedEwtModels.Option('installSpecialAttribute2', sked__Job__c.sked_InstallSpecialAttribute2__c.getDescribe().getLabel()));

        return checkBoxLabels;
    }

    // Convert decimal to binary representation (alas, Apex has no native method :-(
    //    eg. 4 => '100', 19 => '10011', etc.
    // Method: Divide by 2 repeatedly until 0. At each step note the remainder (0 or 1).
    // These, in reverse order, are the binary.
    public static String decimalToBinary(Integer val) {
        String bits = '';
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }

    // Convert a base64 token into a binary/bits representation
    // e.g. 'gAAA' => '100000000000000000000'
    public static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';

        String base64Chars = '' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' + 'abcdefghijklmnopqrstuvwxyz' + '0123456789+/';

        String validForBits = '';

        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }

        return validForBits;
    }

    public static List<skedEwtModels.PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> PLEs) {
        return (List<skedEwtModels.PicklistEntryWrapper>)
            JSON.deserialize(JSON.serialize(PLEs), List<skedEwtModels.PicklistEntryWrapper>.class);
    }

    public static String getRegionIdFromTimezone(String timezone) {
        //List<sked__region__c> skedRegions = [SELECT Id FROM sked__region__c WHERE sked__Timezone__c =: timezone];
        List<sked__region__c> skedRegions = [SELECT Id FROM sked__region__c LIMIT 100];

        String result = '';
        if (skedRegions != null && !skedRegions.isEmpty()) {
            result = skedRegions.get(0).id;
        }

        return result;
    }

    public static Messaging.Singleemailmessage createEmail(String accountName, String customerEmail, String emailTemplateName, String jobStartTime,
                                                            String replyToAddress, OrgWideEmailAddress orgWideEmail) {
        List<EmailTemplate> emailTemplates = [SELECT Id, Subject, HtmlValue, Body, DeveloperName
                                            FROM EmailTemplate
                                            WHERE DeveloperName = :emailTemplateName];

        if (emailTemplates == null || emailTemplates.isEmpty()) {
            throw new skedException(skedConstants.MISSING_EMAIL_TEMPLATE);
        }

        EmailTemplate template =emailTemplates.get(0);

        String subject = template.Subject;

        String htmlBody = template.HtmlValue;
        htmlBody = htmlBody.replace('[Account Name]', accountName);
        htmlBody = htmlBody.replace('[Appt Date and Time]', jobStartTime);

        Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
        email.setToAddresses(new List<String>{customerEmail});
        email.setSaveAsActivity(false);
        email.setReplyTo(replyToAddress);

        email.setSubject(subject);
        email.setHtmlBody(htmlBody);

        if (orgWideEmail != null) {
            email.setorgWideEmailAddressId(orgWideEmail.Id);
        }

        return email;
    }

    public static OrgWideEmailAddress getOrgWideEmail(String admissionEmail) {
        List<OrgWideEmailAddress> orgEmailResult = [
            SELECT Id, Address, DisplayName
            FROM OrgWideEmailAddress
            WHERE Address = :admissionEmail
        ];

        if (!orgEmailResult.isEmpty()) {
            return orgEmailResult[0];
        }

        return null;
    }

    public static String buildLikeSearchString(String term) {
        if (String.isBlank(term)) {
            return '%%';
        }

        return '%' + String.escapeSingleQuotes(term).trim() + '%';
    }

    public static String constructHomeAddress(Address accAddress) {
        if (accAddress == null) {
            return '';
        }
        String homeAdd = String.isBlank(accAddress.Street) ? '' : accAddress.Street ;
        homeAdd += getFormattedAddrString(accAddress.City, false);
        homeAdd += getFormattedAddrString(accAddress.State, false);
        homeAdd += getFormattedAddrString(accAddress.PostalCode, true);
        homeAdd += getFormattedAddrString(accAddress.Country, false);

        homeAdd = homeAdd.trim();

        if(String.isnotBlank(homeAdd)) {
            homeAdd = homeAdd.removeStart(',');
        }

        return homeAdd;
    }

    public static String getFormattedAddrString(String inStr, Boolean isPostCode) {
        return String.isBlank(inStr) ? '' : (isPostCode ? ' ' + inStr  : ',' + inStr);
    }
}