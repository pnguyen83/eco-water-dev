global class skedEwtModels {
	global class DateSlot {
		public List<skedModels.Slot> timeslots;
		public string dayString ;
		public string dayLabel ;
		public integer noOfTimeslot ;
		public string weekday ;
		public boolean isAvailable;

		public DateSlot (DateTime selectedTime, String timezone) {
			this.timeslots = new List<skedModels.Slot>();
			this.dayLabel = selectedTime.format(skedConstants.WEEKDAY_DAY_MONTH, timezone);
			this.weekday = selectedTime.format(skedConstants.WEEKDAY, timezone);
			this.dayString = selectedTime.format(skedDateTimeUtils.DATE_ISO_FORMAT, timezone);
		}
	}

	global class BookingConfigData {
		public boolean isMarketer;
		public List<Option> jobtypes;
		public Map<String, List<String>> map_source_sourceDetail;
		public List<Option> daysOfWeek ;
		public Map<String, List<skedModels.Slot>> weekday_timeslots;
		public List<skedModels.product> products;
		public List<Option> urgencyTypes;
		public List<skedModels.region> regions;
		public List<Option> installationCheckBoxName;

		public BookingConfigData() {
			String resType = skedCommonService.getResourceCategoryFromUserId();
			this.isMarketer = resType.equalsIgnoreCase(skedConstants.RES_TYPE_MARKETER);
			this.jobTypes = skedCommonService.getPickListValues('sked__Job__c', 'sked__Type__c');
			this.urgencyTypes = skedCommonService.getPickListValues('sked__Job__c', 'sked__Urgency__c');
			this.map_source_sourceDetail = skedCommonService.getDependentOptionsImpl('Account', 'sked_Source__c', 'sked_Source_Detail__c');

			this.daysOfWeek = new List<Option>();
			this.daysOfWeek.add(new Option('Mon', 'Monday'));
			this.daysOfWeek.add(new Option('Tue', 'Tuesday'));
			this.daysOfWeek.add(new Option('Wed', 'Wednesday'));
			this.daysOfWeek.add(new Option('Thu', 'Thursday'));
			this.daysOfWeek.add(new Option('Fri', 'Friday'));
			this.daysOfWeek.add(new Option('Sat', 'Saturday'));
			this.daysOfWeek.add(new Option('Sun', 'Sunday'));

			this.products = skedCommonService.getProducts();
			this.regions = skedCommonService.getRegions();
			this.installationCheckBoxName = skedCommonService.getCheckBoxLabel();
		}

	}

	global class BookingGridData {
		public skedModels.contact contactInfo;
		public List<ContactPrimaryDetail> contactPrimaryDetails;
		public String jobType;
		public skedModels.region selectedRegion;
		public String selectedDate;
		public List<String> selectedWeekDays;
		public List<String> selectedTimeSlots;
		public integer jobStarTime;
		public integer jobEndTime;
		public skedModels.slot selectedSlot;
		public String contactId;
		public String accountId;
		public String accountName;
		public String jobId;
		public String selectedSource;
		public String selectedSourceDetail;
		public String timezone;

		//public List<skedResourceAvailabilityBase.resourceModel> resources;

		public List<skedModels.product> products;
		public Boolean siteSurvey;
		public Boolean digTess;
		public Boolean permit;
		public Boolean garageManifold;
		public Boolean exteriorManifold;
		public Boolean main;
		public Boolean trench;
		public Boolean graniteDrill;
		//EWT-149
		public Boolean shedExteriorPlacement;
		public Boolean driveBore;
		public Boolean iceLine;
		public Boolean installSpecialAttribute1;
		public Boolean installSpecialAttribute2;

		public String urgencyType;
	}

	global class SlotRequestData {
		public String jobType;
		public String selectedDate;
	}

	global class Option extends skedModels.modelBase{
		public Option (String id, String name) {
			this.id = id;
			this.name = name;
		}
	}

	global class PicklistEntryWrapper {
		public String active ;
		public String defaultValue;
		public String label {get; set;}
		public String value {get; set;}
		public String validFor {get; set;}
	}

	global class ContactPrimaryDetail {
		public String name;
		public String phone;
		public String email;
		public boolean isPrimaryContact;
		public Boolean isCellPhone;
	}

	global class SlotFilter {
		public String startTime;
		public String endTime;
		public String startDate;
		public String endDate;
		public String timezone;
		public String jobType;
		public boolean isConfigData;

		public SlotFilter(DateTime start, DateTime finish, String timezone, String jobtype) {
			this.isConfigData = true;
			if (start != null && finish != null && String.isNotBlank(timezone)) {
				this.startTime = start.format(skedConstants.DATE_TIME_IN_QUERY, timezone);
				this.endTime = finish.format(skedConstants.DATE_TIME_IN_QUERY, timezone);
				this.startDate = start.format(skedConstants.YYYY_MM_DD, timezone);
				this.endDate = finish.format(skedConstants.YYYY_MM_DD, timezone);
				this.timezone = timezone;
				this.isConfigData = false;
			}
			this.jobType = jobType;
		}
	}

	global class SearchModel {
		public String fieldName;
		public String value;
	}
}