public class skedConstants {
    public static final string ACCCOUNT_SCORE_BLACKLIST = 'Do not schedule';
    public static final string ACCCOUNT_SCORE_WHITELIST = 'Preferred';

    public static final string AVAILABILITY_STATUS_APPROVED = 'Approved';
    public static final string AVAILABILITY_STATUS_PENDING = 'Pending';
    public static final string AVAILABILITY_STATUS_DECLINED = 'Declined';

    public static final string JOB_CANCELLATION_REASON_RESCHEDULED = 'Rescheduled';

    public static final string JOB_STATUS_CANCELLED = 'Cancelled';
    public static final string JOB_STATUS_COMPLETE = 'Complete';
    public static final string JOB_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_STATUS_PENDING_ALLOCATION = 'Pending Allocation';
    public static final string JOB_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_STATUS_QUEUED = 'Queued';
    public static final string JOB_STATUS_READY = 'Ready';
    public static final string JOB_STATUS_ON_SITE = 'On Site';
    public static final string JOB_STATUS_EN_ROUTE = 'En Route';

    public static final string JOB_TYPE_DEFAULT = 'Appointment';

    public static final string JOB_ALLOCATION_STATUS_COMPLETE = 'Complete';
    public static final string JOB_ALLOCATION_STATUS_CONFIRMED = 'Confirmed';
    public static final string JOB_ALLOCATION_STATUS_EN_ROUTE = 'En Route';
    public static final string JOB_ALLOCATION_STATUS_CHECKED_IN = 'Checked In';
    public static final string JOB_ALLOCATION_STATUS_DECLINED = 'Declined';
    public static final string JOB_ALLOCATION_STATUS_DELETED = 'Deleted';
    public static final string JOB_ALLOCATION_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_ALLOCATION_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_ALLOCATION_STATUS_IN_PROGRESS = 'In Progress';

    public static final Set<String> CLOSER_JA_STATUS = new Set<String>{JOB_ALLOCATION_STATUS_CONFIRMED, JOB_ALLOCATION_STATUS_EN_ROUTE,
                                                                       JOB_ALLOCATION_STATUS_CHECKED_IN, JOB_ALLOCATION_STATUS_DISPATCHED,
                                                                        JOB_ALLOCATION_STATUS_PENDING_DISPATCH, JOB_ALLOCATION_STATUS_IN_PROGRESS};

    public static final Set<String> RES_STATUS_USE_HOME_ADDRESS = new Set<String>{JOB_ALLOCATION_STATUS_CONFIRMED,JOB_ALLOCATION_STATUS_EN_ROUTE,
                                                                        JOB_ALLOCATION_STATUS_IN_PROGRESS, JOB_ALLOCATION_STATUS_COMPLETE};

    public static final string JOB_OFFER_STATUS_OFFERED = 'Offered';
    public static final string JOB_OFFER_STATUS_ACCEPTED = 'Accepted';
    public static final string JOB_OFFER_STATUS_DECLINED = 'Declined';
    public static final string JOB_OFFER_STATUS_CANCELLED = 'Cancelled';

    public static final string JOB_TYPE_INITIAL_EVALUATION = 'Initial Evaluation';
    public static final string JOB_TYPE_TREATMENT = 'Treatment';
    public static final string JOB_TYPE_GROUP_EVENT = 'Group Event';

    public static final string RESOURCE_TYPE_PERSON = 'Person';
    public static final string RESOURCE_TYPE_ASSET = 'Asset';

    public static final string PERIOD_WEEK         = 'week';
    public static final string PERIOD_2WEEKS       = '2 weeks';
    public static final string PERIOD_MONTH        = 'month';
    public static final string PERIOD_FORTNIGHT    = 'Current Fortnightly Roster';

    public static final string OPPORTUNITY_STAGE_SCHEDULED_EVALUATION = 'Scheduled Evaluation';

    public static final string HOLIDAY_GLOBAL = 'global';

    public static final string EXCEPTION_CONCURRENCY = 'CONCURRENCY_ERROR';
    public static final string EXCEPTION_CONCURRENCY_PATIENT = 'CONCURRENCY_PATIENT_ERROR';
    public static final string JOB_HAS_NO_RESOURCE_ERROR = 'Cannot complete a job that is not assigned to any resource';

    public static final string CARE_PLAN_RECORD_TYPE = 'Care Plan Task';

    //format day label on setter booking modal
    public static final string WEEKDAY_DAY_MONTH = 'EEE d MMM';
    public static final string WEEKDAY = 'EEE';
    public static final string WEEKDAY_DAY_MONTH_YEAR = 'h:mma EEEEE d MMM yyyy';
    public static final string HOUR_ONLY = 'ha';
    public static final String HOUR_MINUTES_ONLY = 'h:mma';
    public static final String YYYY_MM_DD = 'yyyy-MM-dd';
    public static final String DATE_TIME_IN_QUERY = 'yyyy-MM-dd\'T\'hh:mm:ss\'Z\'';

    //error message when Access Id is not available
    public static final string ACCESS_ID_NOT_AVAI = 'Invalid Access Id or User is in-active';

    public static final String RESOURCE_DOES_NOT_HAVE_USER = 'Current login user is not associated with any active resource';

    //error message when location is missing geo location
    public static final String USER_NOT_ASSOCIATED_WITH_RES = 'The current login user is not associated with any resource';

    public static final String INVALID_JOB_ID = 'Invalid Job Id';

    public static final String MISSING_RESCHEDULE_DAY = 'Missing Rescheduled Day';

    public static final String MISSING_REGION = 'Current Login user is not associated with any region';

    public static final String MISSING_SOURCE = 'Missing Source';

    public static final String MISSING_TAG = 'Associated tag not found in system';

    public static final String ASSOCIATED_RESOURCE_NOT_ACTIVE = 'The associated resource of current login user is not active';

    //Email template developer name
    public static final String EMAIL_CONSULTATION_TEMPLATE = 'sked_Appointment_Confirmation';
    public static final String EMAIL_INSTALLATION_TEMPLATE = 'sked_Installation_Scheduled';

    //Email Template error
    public static final String MISSING_EMAIL_TEMPLATE = 'Missing Email Template';

    //Email
    public static final String EMAIL_SENDER_NAME = 'EcoWater Texas';
    public static final String EMAIL_CONSULTATION_ADDRESS = 'appointments@ecowatertexas.com';
    public static final String EMAIL_INSTALLATION_ADDRESS = 'install@ecowatertexas.com';
    public static final String EMAIL_SERVICE_ADDRESS = '';

    //resource types
    public static final String RES_TYPE_MARKETER = 'Marketer';
    public static final String RES_TYPE_TECHNICIAN = 'Installation Technician';

    //job types
    public static final String JOB_TYPE_CONSULTATION = 'Consultation';
    //public static final String JOB_TYPE_INSTALLATION = 'Installation';
    public static final String JOB_TYPE_INSTALLATION_WH = 'Installation-WH';
    public static final String JOB_TYPE_INSTALLATION_RO = 'Installation-RO';
    public static final String JOB_TYPE_SERVICE = 'Service';

    // search Account
    public static final String SEARCH_ACCOUNT_BY_PHONE = 'Phone';
    public static final String SEARCH_ACCOUNT_BY_NAME = 'Name';
    public static final String SEARCH_ACCOUNT_BY_ADDRESS = 'Address';
    public static final String SEARCH_ACCOUNT_BY_ID = 'Id';

    // permission set
    public static final String INSTALLTION_PERMISSION_SET = 'Installation';
    public static final String SYSTEM_ADMIN_PROFILE_NAME = 'System Administrator';

}