@isTest
public class skedBookingHandlerTest {
    @isTest static void test_getConfigData() {
        Map<string, sObject> mapObject = skedDataSetup.setupCommonTestData();

        Test.startTest();
        skedRemoteResultModel result = skedBookingController.getConfigData();
        Test.stopTest();
    }

    @isTest static void test_getSlot() {
        Map<string, sObject> mapObject = skedDataSetup.setupCommonTestData();
        String timezone = UserInfo.getTimeZone().getId();
        String selectedDate = system.now().format(skedDateTimeUtils.DATE_ISO_FORMAT, timezone);

        Test.startTest();

        skedEwtModels.SlotRequestData slotRequest = new skedEwtModels.SlotRequestData();
        slotRequest.jobType = skedConstants.JOB_TYPE_CONSULTATION;
        slotRequest.selectedDate = selectedDate;

        skedRemoteResultModel result = skedBookingController.getSlot(slotRequest);
        System.debug('result.data = ' + result.data);

        Test.stopTest();
    }

    @isTest static void test_getBookingGridData() {
        Map<string, sObject> mapObject = skedDataSetup.setupCommonTestData();
        skedEwtModels.BookingGridData data = buildApointmentData(skedConstants.JOB_TYPE_CONSULTATION, mapObject);


        Test.startTest();
        skedRemoteResultModel result = skedBookingController.getBookingGridData(data);
        Test.stopTest();
    }

    @isTest static void test_createAppointmentConsultaion() {
        Map<string, sObject> mapObject = skedDataSetup.setupCommonTestData();
        skedEwtModels.BookingGridData data = buildApointmentData(skedConstants.JOB_TYPE_CONSULTATION, mapObject);


        Test.startTest();

        skedRemoteResultModel result = skedBookingController.createAppointment(data);
        sked__Job__c job = (sked__Job__c)result.data;
        skedRemoteResultModel result2 = skedBookingController.getJobAccount(job.Id);
        String result3 = skedBookingController.getJobDetail(job.Id);
        skedRemoteResultModel result4 = skedBookingController.getJobDetail2(job.Id);

        skedBookingController.getBookingGridData(data);

        Test.stopTest();
    }
    @isTest static void test_createAppointmentInstallation() {
        Map<string, sObject> mapObject = skedDataSetup.setupCommonTestData();
        skedEwtModels.BookingGridData data = buildApointmentData(skedConstants.JOB_TYPE_INSTALLATION_WH, mapObject);

        Test.startTest();
        skedRemoteResultModel result = skedBookingController.createAppointment(data);

        Test.stopTest();

    }

    @isTest static void test_createAppointmentService() {
        Map<string, sObject> mapObject = skedDataSetup.setupCommonTestData();
        skedEwtModels.BookingGridData data = buildApointmentData(skedConstants.JOB_TYPE_SERVICE, mapObject);

        // test with region null and missing tag
        Test.startTest();
        data.selectedRegion = null;
        data.products[0].productType = data.products[0].productType + 'missingTag';
        skedRemoteResultModel result = skedBookingController.createAppointment(data);

        Test.stopTest();

    }

    @isTest static void test_Exception() {
        Map<string, sObject> mapObject = skedDataSetup.setupCommonTestData();
        skedEwtModels.BookingGridData data = buildApointmentData(skedConstants.JOB_TYPE_SERVICE, mapObject);

        // test with region null and missing tag
        data.selectedRegion = null;
        List<sked__Resource__c> resources = [SELECT Id, Name FROM sked__Resource__c];
        if (resources != null && !resources.isEmpty()) {
            delete resources;
        }
        List<sked__Region__c> regions = [SELECT Id, Name FROM sked__Region__c];
        if (regions != null && !regions.isEmpty()) {
            delete regions;
        }

        Test.startTest();
        skedRemoteResultModel result = skedBookingController.createAppointment(data);
        Test.stopTest();

    }

    @isTest static void test_searchAccount() {
        Map<string, sObject> mapObject = skedDataSetup.setupCommonTestData();
        skedEwtModels.SearchModel searchRequest =  new skedEwtModels.SearchModel();

        Test.startTest();

        searchRequest.fieldName = skedConstants.SEARCH_ACCOUNT_BY_NAME;
        searchRequest.value = 'test';
        skedRemoteResultModel result = skedBookingController.searchAccount(searchRequest);

        searchRequest.fieldName = skedConstants.SEARCH_ACCOUNT_BY_PHONE;
        searchRequest.value = '012';
        result = skedBookingController.searchAccount(searchRequest);

        Test.stopTest();
    }

    @isTest static void test_getURL() {
        Test.startTest();
        skedHttpCalloutMock googleRespond = new skedHttpCalloutMock('');
        Test.setMock(HttpCalloutMock.class, googleRespond);

        skedRemoteResultModel result = skedBookingController.getURL('https://maps.googleapis.com/maps/api/place/autocomplete');
        Test.stopTest();
    }

    @isTest static void test_isInstalltionUser() {
        Test.startTest();
        String result = skedBookingController.isInstalltionUser();
        skedRemoteResultModel result2 = skedBookingController.isInstalltionUser2();
        Test.stopTest();
    }


    ////////////////////////////////////////method///////////////////////////////////////
    public static skedEwtModels.BookingGridData buildApointmentData(String jobType, Map<string, sObject> mapObject) {
        skedEwtModels.BookingGridData data = new skedEwtModels.BookingGridData();
        String timezone = UserInfo.getTimeZone().getId();
        String selectedDate = system.now().addDays(1).format(skedDateTimeUtils.DATE_ISO_FORMAT, timezone);
        String selectedWeekDay =  system.now().addDays(1).format(skedConstants.WEEKDAY, timezone);
        System.debug('###selectedWeekDay = ' + selectedWeekDay);

        data.jobType = jobType;
        data.contactInfo = new skedModels.contact();
        data.contactInfo.description = 'Consultation createAppointment test';
        data.contactInfo.address = new skedModels.address();
        data.contactInfo.address.fullAddress = '100 Congress Street Northeast, Washington D.C., DC 20002, USA';
        data.contactInfo.address.geometry = new skedModels.geometry(38.903853, -77.00277799999999);

        data.contactPrimaryDetails = new List<skedEwtModels.ContactPrimaryDetail>();
        skedEwtModels.ContactPrimaryDetail contPri = new skedEwtModels.ContactPrimaryDetail();
        contPri.isPrimaryContact = true;
        contPri.name = 'Test Contact';
        contPri.phone = '(012) 312-3123';
        contPri.isCellPhone = true;
        contPri.email = 'test@skedulotest.com';
        data.contactPrimaryDetails.add(contPri);

        data.jobStarTime = 480;
        data.jobEndTime = 600;
        data.selectedRegion = new skedModels.region((sked__Region__c)mapObject.get('regionTex'));

        Map<String, List<String>> map_source_sourceDetail = skedCommonService.getDependentOptionsImpl('Account', 'sked_Source__c', 'sked_Source_Detail__c');
        if (!map_source_sourceDetail.isEmpty()) {
            List<String> sources = new List<String>(map_source_sourceDetail.keySet());
            List<String> sourcesDetail = map_source_sourceDetail.get(sources[0]);
            data.selectedSource = sources[0];
            data.selectedSourceDetail = sourcesDetail[0];
        }

        data.selectedDate = selectedDate;
        data.selectedWeekDays = new List<String>{'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'};

        data.selectedTimeSlots = new List<String>();
        skedEwtModels.SlotRequestData slotRequest = new skedEwtModels.SlotRequestData();
        slotRequest.jobType = jobType;
        slotRequest.selectedDate = selectedDate;
        skedRemoteResultModel result = skedBookingController.getSlot(slotRequest);
        Map<String, List<skedModels.Slot>> slotsMap = (Map<String, List<skedModels.Slot>>)result.data;
        for (String key : slotsMap.keySet()) {
            List<skedModels.Slot> slots = slotsMap.get(key);
            for (skedModels.Slot slot : slots) {
                data.selectedTimeSlots.add(slot.id);
            }
        }
        List<sked__Slot__c> slots = [
            SELECT Id, Name, sked_Job_Type__c, sked_Day__c
            FROM sked__Slot__c
            WHERE sked_Job_Type__c = :jobType
            AND sked_Day__c = :selectedWeekDay
        ];
        data.selectedSlot = new skedModels.slot();
        data.selectedSlot.id = slots[0].Id;

        if (skedConstants.JOB_TYPE_INSTALLATION_WH.equalsIgnoreCase(jobType)) {
            data.digTess = true;
            data.siteSurvey = false;
            data.permit = false;
            data.garageManifold = false;
            data.exteriorManifold = false;
            data.main = false;
            data.trench = false;
            data.graniteDrill = false;

            data.products = getProductsByJobType(jobType);
        }

        if (skedConstants.JOB_TYPE_SERVICE.equalsIgnoreCase(jobType)) {
            List<skedEwtModels.Option> urgencyTypes = skedCommonService.getPickListValues('sked__Job__c', 'sked__Urgency__c');
            data.products = getProductsByJobType(jobType);
            if (urgencyTypes != null && !urgencyTypes.isEmpty()) {
                data.urgencyType = urgencyTypes[0].name;
            }
        }

        return data;
    }

    public static List<skedModels.product> getProductsByJobType(String jobType) {
        List<skedModels.product> products = new List<skedModels.product>();
        List<Product2> skedProducts = [ SELECT Id, Name, Family, sked_Product_Job_Type__c
                                FROM Product2
                                WHERE sked_Product_Job_Type__c = :jobType
                                LIMIT 100];
        if (skedProducts != null && !skedProducts.isEmpty()) {
            products.add(new skedModels.product(skedProducts[0]));
        }

        return products;
    }
}