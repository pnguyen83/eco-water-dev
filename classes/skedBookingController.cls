global class skedBookingController {
	@remoteaction
	global static skedRemoteResultModel getConfigData() {
		return skedBookingHandler.getConfigData();
	}

	@remoteaction
	global static skedRemoteResultModel getSlot(skedEwtModels.SlotRequestData slotRequest) {
		return skedBookingHandler.getSlot(slotRequest);
	}

	@remoteaction
	global static skedRemoteResultModel getBookingGridData(skedEwtModels.BookingGridData data) {
		return skedBookingHandler.getBookingGridData(data);
	}

	@remoteaction
	global static skedRemoteResultModel createAppointment(skedEwtModels.BookingGridData data) {
		return skedBookingHandler.createAppointment(data);
	}

	@remoteaction
	global static skedRemoteResultModel getJobAccount(String jobId) {
		return skedBookingHandler.getJobAccount(jobId);
	}

	@remoteaction
	global static skedRemoteResultModel searchAccount(skedEwtModels.SearchModel searchRequest) {
		return skedBookingHandler.searchAccount(searchRequest);
	}

	@remoteaction
	global static skedRemoteResultModel getURL(String url) {
		return skedBookingHandler.getURL(url);
	}

	@remoteaction
	global static skedRemoteResultModel getJobDetail2(String jobId) {
		return skedBookingHandler.getJobDetail(jobId);
	}
	@remoteaction
	global static skedRemoteResultModel isInstalltionUser2() {
		 return skedBookingHandler.isInstalltionUser();
	}

	@AuraEnabled
	global static String getJobDetail(String jobId) {
		skedRemoteResultModel result = skedBookingHandler.getJobDetail(jobId);
		return JSON.serialize(result);
	}

	@AuraEnabled
	global static String isInstalltionUser() {
		skedRemoteResultModel result = skedBookingHandler.isInstalltionUser();
		return JSON.serialize(result);
	}
}