public class skedSearchService {
    private static skedSearchService mInstance;
    public static skedSearchService instance {
        get {
            if (mInstance == null) {
                mInstance = new skedSearchService();
            }
            return mInstance;
        }
    }

    // key is domainObjectName and value is objectApiName
    protected Map<String, String> domainToApi;
    private skedSearchService() {
        this.domainToApi = new Map<String, String>();
        this.domainToApi.put('contact', 'Contact');
        this.domainToApi.put('tag', 'sked__Tag__c');
        this.domainToApi.put('shift', 'sked__Shift__c');
        this.domainToApi.put('resource', 'sked__Resource__c');
        this.domainToApi.put('resourceShift', 'sked__Resource_Shift__c');
        this.domainToApi.put('resourceTag', 'sked__Resource_Tag__c');
        this.domainToApi.put('region', 'sked__Region__c');
        this.domainToApi.put('jobTag', 'sked__Job_Tag__c');
        this.domainToApi.put('jobAllocation', 'sked__Job_Allocation__c');
        this.domainToApi.put('job', 'sked__Job__c');
        this.domainToApi.put('caseModel', 'Case');
        this.domainToApi.put('availability', 'sked__Availability__c');
        this.domainToApi.put('activity', 'sked__Activity__c');
        this.domainToApi.put('account', 'Account');
    }

    public skedRemoteResultModel searchObject (String domainObjectName, String searchString,
                            Map<String, List<String>> conditionObjects) {
        skedRemoteResultModel response = new skedRemoteResultModel();
        try {
            String objectApiName = getObjectApiName(domainObjectName);
            if (objectApiName != null) {

                // build query
                skedMappingConfigContainer.mappingConfigModel mainObjectConfig = skedMappingConfigContainer.instance.getMappingConfig(objectApiName);
                skedQueryBuilder queryBuilder = new skedQueryBuilder();
                queryBuilder.setFromClause(objectApiName);

                for (skedMappingConfigContainer.fieldConfigModel fieldConfig : mainObjectConfig.fieldConfigs) {
                    if (fieldConfig.mappingType != 'value' && fieldConfig.mappingType != 'relatedList') {
                        queryBuilder.addField(fieldConfig.sObjectFieldPath);
                    }
                }
                // modify search string (search by name)
                String newSearchString = buildLikeSearchString(searchString);
                String conditionSearch = 'Name LIKE ' + '\'' + newSearchString + '\'';
                queryBuilder.addCondition(conditionSearch);

                if (conditionObjects != null && !conditionObjects.isEmpty()) {
                    for (String domainFieldName : conditionObjects.keySet()) {
                        String apiFieldName = getObjectFieldName(domainFieldName, mainObjectConfig);
                        if (apiFieldName != null) {
                            String conditionField = apiFieldName + ' IN {0}';
                            queryBuilder.addCondition(conditionField, conditionObjects.get(domainFieldName));
                        }
                    }
                }

                string statement = queryBuilder.toString();
                
                List<sObject> sObjectList = (List<sObject>)Database.query(statement);

                skedAutoMapper autoMap = skedAutoMapper.instance;
                List<Object> results = new List<Object>();
                for (sObject record : sObjectList) {
                    Type domainObjectNameType = Type.forName('skedModels.'+domainObjectName);
                    results.add(autoMap.mapTo(record, domainObjectNameType));
                }

                response.data = results;
                response.success = true;
            }
            else {
                response.errorMessage = 'Can not find domainObjectName: ' + domainObjectName;
            }
        } catch(Exception ex){
            response.getError(ex);
        }
        return response;
    }

    // build a map between objectModelName and objectApiName
    public String getObjectApiName(String domainObjectName) {
        if (domainToApi.containsKey(domainObjectName)) {
            return domainToApi.get(domainObjectName);
        }
        return null;
    }

    public String buildLikeSearchString(String term) {
        if (String.isBlank(term)) {
            return '%%';
        }

        return '%' + String.escapeSingleQuotes(term).trim() + '%';
    }

    public String getObjectFieldName(String domainFieldName, skedMappingConfigContainer.mappingConfigModel mainObjectConfig) {
        for (skedMappingConfigContainer.fieldConfigModel fieldConfig : mainObjectConfig.fieldConfigs) {
            if (fieldConfig.domainFieldName.equalsIgnoreCase(domainFieldName)) {
                return fieldConfig.sObjectFieldPath;
            }
        }
        return null;
    }
}