global class skedSetting {

    private skedAppSetting appSetting;
    private skedAdminSetting adminSetting;

    public skedAppSetting App {
        get {
            if (appSetting == null) {
                appSetting = new skedAppSetting();
            }
            return appSetting;
        }
    }

    public skedAdminSetting Admin {
        get {
            if (adminSetting == null) {
                adminSetting = new skedAdminSetting();
            }
            return adminSetting;
        }
    }

    /*********************************************************** Singleton stuffs ***********************************************************/
    private static skedSetting mInstance = null;

    public static skedSetting instance {
        get {
            if (mInstance == null) {
                mInstance = new skedSetting();
            }
            return mInstance;
        }
    }

    /*********************************************************** Private constructor ***********************************************************/
    private skedSetting() {}

    /*********************************************************** Nested Classes ***********************************************************/
    public class skedAppSetting {
        public string timezoneSidId;
        public boolean daylightSavingDetected;

        public skedAppSetting() {
            this.timezoneSidId = UserInfo.getTimeZone().getID();
            this.daylightSavingDetected = true;
        }
        /*
        public void setTimezoneSidId(string inputTimezoneSidId) {
            this.timezoneSidId = inputTimezoneSidId;
        }

        public void detectDaylightSaving(Date startDate, Date endDate) {
            detectDaylightSaving(DateTime.newInstance(startDate, Time.newInstance(0, 0, 0, 0)), DateTime.newInstance(endDate, Time.newInstance(0, 0, 0, 0)));
        }

        public void detectDaylightSaving(DateTime startTime, DateTime endTime) {
            Timezone tz = Timezone.getTimezone(this.timezoneSidId);
            this.daylightSavingDetected = tz.getOffset(startTime) != tz.getOffset(endTime);
        }
        */
    }

    public class skedAdminSetting {
        public string distanceMeasurementUnit;
        public integer velocity;
        public boolean ignoreTravelTimeFirstJob;
        public Map<String, Decimal> map_jobtype_defaultDuration;

        public skedAdminSetting() {
            this.distanceMeasurementUnit = 'mi';
        	this.velocity = 30;
            this.ignoreTravelTimeFirstJob = true;
            this.map_jobtype_defaultDuration = new Map<String, Decimal>();

            for (Sked_Job_Time_Management__mdt jobTime : [SELECT Id, Label, sked_Default_Duration__c
                                                            FROM Sked_Job_Time_Management__mdt]) {
                this.map_jobtype_defaultDuration.put(jobTime.Label, jobTime.sked_Default_Duration__c);
            }
        }
    }

}