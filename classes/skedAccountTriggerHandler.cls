public class skedAccountTriggerHandler {
	public static void afterInsert(List<sObject> oldRecords, Map<Id, sObject> newMap) {
        if(!System.isFuture() && !System.isBatch()) skedLocationServices.updateLocationGeocode(oldRecords, newMap, skedLocationServices.OBJ_ACCOUNT);
    }

    public static void afterUpdate(List<sObject> oldRecords, Map<Id, sObject> newMap) {
        system.debug('sked::accout.afterUpdate');
        if(!System.isFuture() && !System.isBatch()) skedLocationServices.updateLocationGeocode(oldRecords, newMap, skedLocationServices.OBJ_ACCOUNT);
    }
}