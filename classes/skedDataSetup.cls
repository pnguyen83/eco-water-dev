@isTest
public class skedDataSetup {

    public static Map<string, sObject> setupCommonTestData() {

        DateTime current = system.now();
        Date currentData = system.now().date();

        Map<string, sObject> mapTestData = new Map<string, sObject>();

        /*********************************************************1st Level**************************************************/
        List<sObject> firstList = new List<sObject>();

        /*********************************************************Custom settings**************************************************/
        skedConfigs__c config = new skedConfigs__c(
            Marketing_Email__c = 'maketing@test.com',
            Sales_Email__c = 'sales@test.com'
            );
        firstList.add(config);

        /*********************************************************Region**************************************************/
        sked__Region__c regionTex = new sked__Region__c(
            Name = 'Texas',
            sked__Timezone__c = 'America/Chicago'
        );

        firstList.add(regionTex);
        mapTestData.put('regionTex', regionTex);

        /*********************************************************Tag**************************************************/
        Map<String, List<String>> map_source_sourceDetail = skedCommonService.getDependentOptionsImpl('Account', 'sked_Source__c', 'sked_Source_Detail__c');
        List<sked__Tag__c> tagSourceList = new List<sked__Tag__c>();
        for (String tagName : map_source_sourceDetail.keySet()) {
            tagSourceList.add(new sked__Tag__c(Name = tagName, sked__Type__c = 'Skill', sked__Classification__c = 'Global'));
        }

        firstList.addAll((List<sObject>)tagSourceList);

        List<skedEwtModels.Option> productTypes = skedCommonService.getPickListValues('Product2', 'Family');
        List<sked__Tag__c> tagProductList = new List<sked__Tag__c>();
        for (skedEwtModels.Option productType : productTypes) {
            tagProductList.add(new sked__Tag__c(Name = productType.name, sked__Type__c = 'Skill', sked__Classification__c = 'Global'));
        }

        firstList.addAll((List<sObject>)tagProductList);

        /*********************************************************Product**************************************************/
        List<Product2> products = new List<Product2>();
        List<skedEwtModels.Option> jobProductTypes = skedCommonService.getPickListValues('Product2', 'sked_Product_Job_Type__c');
        for (skedEwtModels.Option jobProductType : jobProductTypes) {
            for (skedEwtModels.Option productType : productTypes) {
                products.add(new Product2( Name = productType.name + '-' + jobProductType.name,
                                            Family = productType.name,
                                            sked_Product_Job_Type__c = jobProductType.name));
            }

        }

        firstList.addAll((List<sObject>)products);

        /*********************************************************Slot**************************************************/
        List<sked__Slot__c> slots = new List<sked__Slot__c>();
        Date slotActiveStartDate = currentData.addDays(-1);
        Date slotActiveEndDate = currentData.addDays(30);
        List<String> weekDays = new List<String>{'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'};

        for (skedEwtModels.Option jobProductType : jobProductTypes) {
            for (String weekDay : weekDays) {
                slots.add(new sked__Slot__c(
                            Name = weekDay + ' 8AM - 10AM',
                            sked__Quantity__c = 2,
                            sked_Day__c = weekDay,
                            sked_Job_Type__c = jobProductType.name,
                            sked_Slot_Active_Start_Date__c = slotActiveStartDate,
                            sked_Slot_Active_End_Date__c = slotActiveEndDate,
                            sked_Slot_Start_Time__c = 800,
                            sked_Slot_End_Time__c = 1000));
            }
        }
        firstList.addAll((List<sObject>)slots);

        /*********************************************************Accounts**************************************************/
        List<String> sources = new List<String>(map_source_sourceDetail.keySet());
        List<String> sourceDetail = map_source_sourceDetail.get(sources[0]);
        Account acc = new Account(
                    Name = 'test searchName',
                    BillingStreet = 'test searchAddress 907 San Jacinto Blvd, Austin, TX 78701, USA',
                    BillingLatitude = 37.0328374,
                    BillingLongitude = -95.73102589999999,
                    sked_Source__c = sources[0],
                    sked_Source_Detail__c = sourceDetail[0]
                );
        firstList.add(acc);
        mapTestData.put('acc', acc);

        insert firstList;

        /*********************************************************2st Level**************************************************/
        List<sObject> secondList = new List<sObject>();
        /*********************************************************Contacts**************************************************/
        Contact cont = new Contact (
                LastName = 'test searchName',
                Phone = '(012) 345-6789',
                MailingStreet = 'test searchAddress 907 San Jacinto Blvd, Austin, TX 78701, USA',
                AccountId = acc.Id,
                sked_Primary_Contact__c = true
            );

        secondList.add(cont);
        mapTestData.put('cont', cont);

        /*********************************************************Resource**************************************************/
        List<skedEwtModels.Option> resourceTypes = skedCommonService.getPickListValues('sked__Resource__c', 'sked__Category__c');

        sked__Resource__c resource = new sked__Resource__c(
            Name = resourceTypes[0].name,
            sked__Primary_Region__c = regionTex.Id,
            sked__Category__c = resourceTypes[0].name,
            sked__Resource_Type__c = 'Person',
            sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
            sked__GeoLocation__Latitude__s = -27.457730,
            sked__GeoLocation__Longitude__s = 153.037080,
            sked__User__c = UserInfo.getUserId(),
            sked__Is_Active__c = TRUE
        );
        secondList.add(resource);
        mapTestData.put('resource', resource);

        insert secondList;

        return mapTestData;
    }

            /*
        sked__Tag__c tagCostco = new sked__Tag__c(
            Name = 'Costco',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        mapTestData.put('tagCostco', tagCostco);
        */
        /*
        sked__Tag__c tagWHOnly = new sked__Tag__c(
            Name = 'WH-Only',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        firstList.add(tagWHOnly);
        mapTestData.put('tagWHOnly', tagWHOnly);

        sked__Tag__c tagMaintenance = new sked__Tag__c(
            Name = 'Maintenance',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        mapTestData.put('tagMaintenance', tagMaintenance);
        */
        /*
        Product2 productInstallation = new Product2(
            Name = 'Test Product Installation',
            Family = 'WH-Only',
            sked_Product_Job_Type__c = 'Installation-WH'

        );
        firstList.add(productInstallation);
        mapTestData.put('productInstallation', productInstallation);

        Product2 productService = new Product2(
            Name = 'Test Product Service',
            Family = 'Maintenance',
            sked_Product_Job_Type__c = 'Service'

        );
        firstList.add(productService);
        mapTestData.put('productService', productService);
        */
        /*
        sked__Slot__c slotMonConsult = new sked__Slot__c(
            Name = 'Mon 8AM - 10AM',
            sked__Quantity__c = 2,
            sked_Day__c = 'Mon',
            sked_Job_Type__c = 'Consultation',
            sked_Slot_Active_Start_Date__c = slotActiveStartDate,
            sked_Slot_Active_End_Date__c = slotActiveEndDate,
            sked_Slot_Start_Time__c = 800,
            sked_Slot_End_Time__c = 1000
            );

        firstList.add(slotMonConsult);
        mapTestData.put('slotMonConsult', slotMonConsult);

        sked__Slot__c slotMonInstall = new sked__Slot__c(
            Name = 'Mon 8AM - 10AM',
            sked__Quantity__c = 2,
            sked_Day__c = 'Mon',
            sked_Job_Type__c = 'Installation-WH',
            sked_Slot_Active_Start_Date__c = slotActiveStartDate,
            sked_Slot_Active_End_Date__c = slotActiveEndDate,
            sked_Slot_Start_Time__c = 800,
            sked_Slot_End_Time__c = 1000
            );

        firstList.add(slotMonInstall);
        mapTestData.put('slotMonInstall', slotMonInstall);

        sked__Slot__c slotMonService = new sked__Slot__c(
            Name = 'Mon 8AM - 10AM',
            sked__Quantity__c = 2,
            sked_Day__c = 'Mon',
            sked_Job_Type__c = 'Service',
            sked_Slot_Active_Start_Date__c = slotActiveStartDate,
            sked_Slot_Active_End_Date__c = slotActiveEndDate,
            sked_Slot_Start_Time__c = 800,
            sked_Slot_End_Time__c = 1000
            );

        firstList.add(slotMonService);
        mapTestData.put('slotMonService', slotMonService);
        */
}