global class skedModels {

    global virtual class modelBase {
        public string id;
        public string name;
        public boolean selected;
        public string action;
    }

    //global virtual class event extends modelBase implements Comparable {
    global virtual class event extends modelBase{
        public string objectType;
        public string eventType;
        public address address;
        public string startDate;
        public string endDate;
        public integer startTime;
        public integer endTime;
        public decimal duration;
        public transient DateTime start;
        public transient DateTime finish;
        public boolean isAvailable;

        public transient string timezoneSidId;
        public transient Date eventDate;
        public transient Location geoLocation;
        /*
        public virtual Integer compareTo(Object compareTo) {
            event compareToRecord = (event)compareTo;
            Integer returnValue = 0;

            if (start > compareToRecord.start) {
                returnValue = 1;
            }
            else if (start < compareToRecord.start) {
                returnValue = -1;
            }
            return returnValue;
        }
        */
    }

    global virtual class address {
        public string fullAddress;
        public string postalCode;
        public geometry geometry;
    }

    global virtual class geometry {
        public decimal lat;
        public decimal lng;
        /*
        public geometry(Location geoLocation) {
            if (geoLocation != null) {
                this.lat = geoLocation.getLatitude();
                this.lng = geoLocation.getLongitude();
            }
        }
        */

        public geometry(decimal lat, decimal lng) {
            this.lat = lat;
            this.lng = lng;
        }
    }

    global virtual class journey {
        public geometry origin;
        public geometry destination;
        public integer travelTime;
        public decimal distance;
        public string status;
    }

    /************************************************ SALESFORCE OBJECT MODELS ************************************************/
    global virtual class caseModel extends modelBase {
        public string contactId;
        public string caseNumber;
    }

    global virtual class contact extends modelBase {
        public string accountId;
        public address address;
        public string birthdate;
        public string email;
        public string firstName;
        public boolean isPrimary;
        public string lastName;
        public string phone;
        public string description;
    }

    global virtual class account extends modelBase {
        public string phone;
        public Boolean isCellPhone;
        public address address;
        public string primaryContactId;
    }

    global virtual class product extends modelBase {
        public string productType;
        public string productJobType;
        public product (Product2 prod) {
            this.id = prod.Id;
            this.name = prod.Name;
            this.productType = prod.Family;
            this.productJobType = prod.sked_Product_Job_Type__c;
        }
    }

    /************************************************ SKEDULO MANAGED OBJECT MODELS ************************************************/
    global virtual class accountResourceScore {
        public string accountId;
        public string resourceId;
        public boolean whitelisted;
        public boolean blacklisted;
    }

    global virtual class activity extends event {
        public string resourceId;
        public string notes;
        public string scheduleId;
    }

    global virtual class availability extends event {
        public string resourceId;
        public string notes;
        public boolean isAvailable;
        public string status;
        public string scheduleId;
        public boolean isAllDay;
    }

    global virtual class holiday extends modelBase {
        public string startDate;
        public string endDate;
        public boolean isGlobal;
        public string regionId;
    }

    global virtual class job extends event {
        public string accountId;
        public string caseId;
        public string contactId;
        public string description;
        public decimal jobAllocationCount;
        public string notes;
        public decimal quantity;
        public string rescheduleJobId;
        public string regionId;
        public string scheduleId;
        public recurringSchedule schedule;
        public string serviceLocationId;
        public List<job> followups;
        public List<jobAllocation> jobAllocations;
        public List<jobTag> jobTags;

        public string jobType;
        public job () {
        }
        public job (sked__Job__c skedJob) {
            this.id = skedJob.id;
            this.name = skedJob.name;
            this.start = skedJob.sked__Start__c;
            this.finish = skedJob.sked__Finish__c;
        }
    }

    global virtual class jobAllocation extends event {
        public string jobId;
        public string resourceId;

        public job job;
        public resource resource;
    }

    global virtual class jobTag extends modelBase {
        public string jobId;
        public string tagId;
        public tag tag;
    }

    global virtual class resource extends modelBase {
        public string category;
        public string photoUrl;
        public string regionId;
        public string userId;
        public decimal rating;
        public address address;
        public List<activity> activities;
        public List<availability> availabilities;
        public List<jobAllocation> allocations;
        public List<resourceTag> resourceTags;
        public List<resourceShift> resourceShifts;

        public List<event> availability;
        public List<event> events;

        public string regionName;
        public integer noOfAvailableJobs;
        public transient Location geoLocation;
    }

    global virtual class resourceTag extends modelBase {
        public string resourceId;
        public string tagId;
        public string expiryDate;
        public integer expiryTime;
        public DateTime expiry;
        public tag tag;
    }

    global virtual class region extends modelBase {
        public string timezoneSidId;
        public region () {}
        public region (sked__Region__c reg) {
            this.id = reg.Id;
            this.name = reg.Name;
            this.timezoneSidId =  reg.sked__Timezone__c;
        }
    }

    global virtual class recurringSchedule extends modelBase {
        public string templateId;
        public string description;
        public boolean skipHolidays;
        public availabilityTemplate template;
    }

    global virtual class serviceLocation extends modelBase {
        public string accountId;
        public string regionId;
        public string description;
        public address address;
    }

    global virtual class shift extends event {
        public address address;
        public string description;
        public string locationId;
        public string regionId;

        public List<resourceShift> resourceShifts;
        public List<resourceShift> possibleResourceShifts;
    }

    global virtual class resourceShift extends modelBase {
        public string shiftId;
        public string resourceId;

        public resource resource;
        public shift shift;
    }

    global virtual class tag extends modelBase {
        public string expiryDate;
        public integer expiryTime;
        public transient DateTime expiryDateTime;
    }

    global virtual class availabilityTemplate extends modelBase {
        public string startDate;
        public string endDate;
        public List<availabilityTemplateEntry> entries;
    }

    global virtual class availabilityTemplateEntry extends modelBase {
        public integer weekNo;
        public string weekday;
        public integer startTime;
        public integer endTime;
        public string action;
    }

    global virtual class slot extends modelBase {
        public string weekday;
        public string jobId;
        public string jobType;
        public decimal quantity;
        public string resourceType;
        public transient Date activeStartDate;
        public transient Date activeEndDate;
        public decimal startTime;
        public decimal endTime;
        public boolean isAvai;

        public List<job> jobs;
        public List<resource> resources;
        /*
        public slot deepClone() {
            slot newSlot = new slot();
            newSlot.weekday = this.weekday;
            newSlot.jobId = this.jobId;
            newSlot.jobType = this.jobType;
            newSlot.quantity = this.quantity;
            newSlot.resourceType = this.resourceType;
            newSlot.activeStartDate = this.activeStartDate;
            newSlot.activeEndDate = this.activeEndDate;
            newSlot.startTime = this.startTime;
            newSlot.endTime = this.endTime;
            newSlot.isAvai = this.isAvai;
            newSlot.id = this.id;
            newSlot.name = this.name;
            newSlot.resources = new List<resource>();
            newSlot.jobs = new List<job>(this.jobs);

            return newSlot;
        }
        */
    }

    /************************************************ SKEDULO CUSTOM MODELS ************************************************/
    global virtual class customTypeEvent extends event {
    }



}