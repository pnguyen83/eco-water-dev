public class skedBookingHandler {
	public static skedRemoteResultModel getConfigData() {
		skedRemoteResultModel result = new skedRemoteResultModel();

		try {
			skedEwtModels.BookingConfigData data = new skedEwtModels.BookingConfigData();
			//data.weekday_timeslots = getListTimeSlots(null, null, null, null);
			result.data = data;
		}
		catch (Exception ex) {
			result.getError(ex);
		}

		return result;
	}

    public static skedRemoteResultModel getSlot(skedEwtModels.SlotRequestData slotRequest) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            if (String.isNotBlank(slotRequest.selectedDate) && String.isNotBlank(slotRequest.jobType)) {
                String timezone = UserInfo.getTimeZone().getId();
                Date selectedDate = Date.valueOf(slotRequest.selectedDate);
                DateTime startOfWeek = getStartOfWeek(selectedDate, timezone);
                DateTime endOfWeek = skedDateTimeUtils.addDays(startOfWeek, 6, timezone);
                endOfWeek = skedDateTimeUtils.getEndOfDate(endOfWeek, timezone).addSeconds(-5);

                Map<String, List<skedModels.Slot>> weekday_timeslots = getListTimeSlots(startOfWeek, endOfWeek, timezone, slotRequest.jobType);
                for (String weekDay : weekday_timeslots.keySet()) {
                    List<skedModels.Slot> slots = weekday_timeslots.get(weekDay);
                    for (skedModels.Slot slot : slots) {
                        slot.jobs = null;
                    }
                }
                result.data = weekday_timeslots;
            }
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getJobAccount(String jobId) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            result.data = getAccountFromJob(jobId);
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel searchAccount(skedEwtModels.SearchModel searchRequest) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            result.data = searchAccByNamePhoneAdd(searchRequest);
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getJobDetail(String jobId) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            result.data = getJobDetailById(jobId);
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }
    public static skedRemoteResultModel isInstalltionUser() {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            if (loginUserIsInstallionUser()) {
                result.data = '1';
            }
            else {
                result.data = '0';
            }
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getURL(String url) {
        skedRemoteResultModel result = new skedRemoteResultModel();
        try {
            result.data = getURLData(url);
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }



	public static skedRemoteResultModel getBookingGridData(skedEwtModels.BookingGridData data) {
		skedRemoteResultModel result = new skedRemoteResultModel();

		try {
            List<skedEwtModels.DateSlot> dateSlots = getListBookingGridDateslots(data);
            //remove jobs before return
            for (skedEwtModels.DateSlot dateSlot : dateSlots) {
                for (skedModels.Slot slot : dateSlot.timeslots) {
                    slot.jobs = null;
                }
            }

			result.data = dateslots;
		}
		catch (Exception ex) {
			result.getError(ex);
		}

		return result;
	}

	public static skedRemoteResultModel createAppointment(skedEwtModels.BookingGridData data) {
		skedRemoteResultModel result = new skedRemoteResultModel();

		try {
            if (skedConstants.JOB_TYPE_CONSULTATION.equalsIgnoreCase(data.jobType)) {
                createAccount(data);
                createContact(data);
            }
			result.data = createJob(data);
            // create JobProduct
            createJobProduct(data);

			result.message = createJobTag(data);

		}
		catch (Exception ex) {
			result.getError(ex);
		}

		return result;
	}


	//======================================Private Functions==============================================//
	private static skedModels.account getAccountFromJob (String jobId) {
        List<sked__Job__c> jobs = [
            SELECT Id, sked__Account__c, sked__Account__r.Name, sked__Account__r.Phone,
                sked__Account__r.BillingAddress, sked__Account__r.BillingStreet,
                sked__Contact__c, sked__Contact__r.Phone, sked__Contact__r.sked_Is_Cell_Phone__c,
                sked__GeoLocation__latitude__s, sked__GeoLocation__longitude__s
            FROM sked__Job__c
            WHERE Id = :jobId
        ];
        skedModels.account jobAccount = new skedModels.account();
        if (jobs != null && !jobs.isEmpty()) {
            jobAccount.id = jobs[0].sked__Account__c;
            jobAccount.name = jobs[0].sked__Account__c != null ? jobs[0].sked__Account__r.Name : '';
            jobAccount.phone = jobs[0].sked__Contact__c != null ? jobs[0].sked__Contact__r.Phone : '';
            jobAccount.isCellPhone = jobs[0].sked__Contact__c != null ? jobs[0].sked__Contact__r.sked_Is_Cell_Phone__c : false;
            jobAccount.primaryContactId = jobs[0].sked__Contact__c;
            jobAccount.address = new skedModels.address();
            skedModels.geometry geo = new skedModels.geometry(jobs[0].sked__GeoLocation__latitude__s, jobs[0].sked__GeoLocation__longitude__s);
            jobAccount.address.fullAddress = jobs[0].sked__Account__c != null ? skedCommonService.constructHomeAddress(jobs[0].sked__Account__r.BillingAddress) : '';

            jobAccount.address.geometry = geo;
        }

        return jobAccount;
    }

    private static List<skedModels.account> searchAccByNamePhoneAdd(skedEwtModels.SearchModel searchRequest) {
        String newSearchString = skedCommonService.buildLikeSearchString(searchRequest.value);
        if (skedConstants.SEARCH_ACCOUNT_BY_ID.equalsIgnoreCase(searchRequest.fieldName)) {
            newSearchString = searchRequest.value;
        }

        List<Account> skedAccounts;
        List<Contact> skedContacts;
        List<skedModels.account> accounts = new List<skedModels.account>();

        if (skedConstants.SEARCH_ACCOUNT_BY_PHONE.equalsIgnoreCase(searchRequest.fieldName)) {
            skedContacts = [
            SELECT Id, Name, Phone, sked_Primary_Contact__c, sked_Is_Cell_Phone__c,
                AccountId, Account.Name, Account.BillingAddress, Account.BillingStreet, Account.BillingCity, Account.BillingState,
                Account.BillingPostalCode, Account.BillingCountry, Account.BillingLatitude, Account.BillingLongitude
            FROM Contact
            WHERE sked_Primary_Contact__c = true
            AND Phone LIKE :newSearchString
            LIMIT 100 ];

            for (Contact contact : skedContacts) {
                skedModels.account acc = new skedModels.account();
                acc.id = contact.AccountId;
                acc.name = contact.AccountId != null ? contact.Account.Name : '';
                acc.phone = contact.Phone;
                acc.isCellPhone = contact.sked_Is_Cell_Phone__c;
                acc.primaryContactId = contact.Id;

                acc.address = new skedModels.address();
                if (contact.AccountId != null) {
                    acc.address.fullAddress = skedCommonService.constructHomeAddress(contact.Account.BillingAddress);
                    skedModels.geometry geo = new skedModels.geometry(contact.Account.BillingLatitude, contact.Account.BillingLongitude);
                    acc.address.geometry = geo;
                }
                accounts.add(acc);
            }
        }
        else {
            skedAccounts = [
            SELECT Id, Name, BillingAddress, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
                    BillingLatitude, BillingLongitude, sked_Is_Cell_Phone__c,
                (
                    SELECT Id, Phone, sked_Primary_Contact__c, sked_Is_Cell_Phone__c
                    FROM Contacts
                    WHERE sked_Primary_Contact__c = true
                )
            FROM Account
            WHERE Name LIKE :newSearchString
            OR BillingStreet LIKE :newSearchString
            OR Id = :newSearchString
            LIMIT 100 ];

            for (Account skedAcc : skedAccounts) {
                skedModels.account acc = new skedModels.account();
                acc.id = skedAcc.Id;
                acc.name = skedAcc.Name;
                acc.phone = skedAcc.Contacts != null && !skedAcc.Contacts.isEmpty() ? skedAcc.Contacts[0].Phone : '';
                acc.isCellPhone = skedAcc.Contacts != null && !skedAcc.Contacts.isEmpty() ? skedAcc.Contacts[0].sked_Is_Cell_Phone__c : false;
                acc.primaryContactId = skedAcc.Contacts != null && !skedAcc.Contacts.isEmpty() ? skedAcc.Contacts[0].Id : null;

                acc.address = new skedModels.address();
                acc.address.fullAddress = skedCommonService.constructHomeAddress(skedAcc.BillingAddress);
                skedModels.geometry geo = new skedModels.geometry(skedAcc.BillingLatitude, skedAcc.BillingLongitude);
                acc.address.geometry = geo;

                accounts.add(acc);
            }
        }
        return accounts;
    }

    private static skedModels.job getJobDetailById (String jobId) {
        List<sked__Job__c> jobs = [
            SELECT Id, Name, sked__Type__c
            FROM sked__Job__c
            WHERE Id = :jobId
        ];
        skedModels.job jobDetail = new skedModels.job();
        if (jobs != null && !jobs.isEmpty()) {
            jobDetail.id = jobs[0].Id;
            jobDetail.name = jobs[0].Name;
            jobDetail.jobType = jobs[0].sked__Type__c;
        }
        return jobDetail;
    }

    // return true if user login have installation permisson set
    private static Boolean loginUserIsInstallionUser () {
        Boolean ret = false;
        String userId = UserInfo.getUserId();

        List<Profile> profiles = [SELECT Id, Name FROM Profile WHERE Id=:Userinfo.getProfileId() LIMIT 1];
        if (profiles != null && !profiles.isEmpty()) {
            String MyProFileName = profiles[0].Name;
            if (profiles[0].Name.equalsIgnoreCase(skedConstants.SYSTEM_ADMIN_PROFILE_NAME)) {
                ret = true;
                return ret;
            }
        }

        List<PermissionSetAssignment> permissionSetAssigns = [
            SELECT Id, PermissionSetId, AssigneeId, PermissionSet.Name
            FROM PermissionSetAssignment
            WHERE AssigneeId = :userId
        ];

        for (PermissionSetAssignment permission : permissionSetAssigns) {
            if (permission.PermissionSet.Name.containsIgnoreCase(skedConstants.INSTALLTION_PERMISSION_SET)) {
                ret = true;
                break;
            }
        }

        return ret;
    }


    public static String getURLData (String url) {
        HTTP http = new HTTP();
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(url);
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('GET');
        HTTPResponse resp = http.send(request);

        if (resp.getStatusCode() == 200) {
            //System.debug('resp.getBody() = ' + resp.getBody());
            return resp.getBody();
        }
        else {
            return '';
        }
    }

    private static skedEwtModels.BookingGridData updateRegionAndTimezone(skedEwtModels.BookingGridData data) {
		if (data.selectedRegion == null) {
            String timezone = UserInfo.getTimeZone().getId();
    		String regionId =  skedCommonService.getRegionIdFromTimezone(timezone);

    		if (String.isBlank(regionId)) {
    			throw new skedException (skedConstants.MISSING_REGION);
    		}
			data.selectedRegion = new skedModels.region();
			data.selectedRegion.id = regionID;
			data.selectedRegion.timezoneSidId = timezone;
		}

		return data;
	}

	private static List<skedEwtModels.DateSlot> getListBookingGridDateslots(skedEwtModels.BookingGridData data) {
		List<skedEwtModels.DateSlot> dateslots = new List<skedEwtModels.DateSlot>();

		data = updateRegionAndTimezone(data);

		String timezone = data.selectedRegion.timezoneSidId;

		String regionId = data.selectedRegion.id;

		Date selectedDate = Date.valueOf(data.selectedDate);
		DateTime startOfWeek = getStartOfWeek(selectedDate, timezone);
		DateTime endOfWeek = skedDateTimeUtils.addDays(startOfWeek, 6, timezone);
        endOfWeek = skedDateTimeUtils.getEndOfDate(endOfWeek, timezone).addSeconds(-5);

		Map<String, List<skedModels.Slot>> map_weekday_timeslots = getListTimeSlots(startOfWeek, endOfWeek, timezone, data.jobType);
		set<Date> setInputDates = new Set<Date>();
		DateTime tempStartDate = startOfWeek;

		//get job  Location
		//Location jobLocation = Location.newInstance(data.contactInfo.address.geometry.lat, data.contactInfo.address.geometry.lng);
        //Set<Id> setResIds = skedCommonService.getResourceIdsInRegion(regionId);

		while (tempStartDate < endOfWeek) {
			string weekDay = tempStartDate.format(skedConstants.WEEKDAY, timezone);
			setInputDates.add(skedDateTimeUtils.getDate(tempStartDate, timezone));
			tempStartDate = skedDateTimeUtils.addDays(tempStartDate, 1, timezone);
		}

        /*
		skedAvailatorParams params = new skedAvailatorParams();
        params.timezoneSidId = timezone;
        params.startDate = skedDateTimeUtils.getDate(startOfWeek, timezone);
        params.endDate = skedDateTimeUtils.getDate(endOfWeek, timezone);
        params.resourceIds = setResIds;
        params.inputDates = setInputDates;
        params.regionId = regionId;

        skedResourceAvailability resourceAvailability = new skedResourceAvailability(params);
        Map<Id, skedResourceAvailabilityBase.resourceModel> mapResourceAllType = resourceAvailability.initializeResourceList();
        Map<Id, skedResourceAvailabilityBase.resourceModel> mapResource = new Map<Id, skedResourceAvailabilityBase.resourceModel>();
        System.debug('#####mapResourceAllType = ' + mapResourceAllType);
        // filter resource type for job type
        String resourceCategoryCheck = skedConstants.RES_TYPE_MARKETER;
        if (skedConstants.JOB_TYPE_CONSULTATION.equalsIgnoreCase(data.jobType)) {
            resourceCategoryCheck = skedConstants.RES_TYPE_MARKETER;
        }
        else if (skedConstants.JOB_TYPE_INSTALLATION.equalsIgnoreCase(data.jobType)) {
            resourceCategoryCheck = skedConstants.RES_TYPE_TECHNICIAN;
        }
        for (skedResourceAvailabilityBase.resourceModel resource : mapResourceAllType.values()) {
            if (resourceCategoryCheck.equalsIgnoreCase(resource.category)) {
                mapResource.put(resource.id, resource);
            }
        }
        System.debug('#####mapResource = ' + mapResource);
        */

		DateTime tempDate = startOfWeek;

		while (tempDate < endOfWeek) {
       		Date checkDate = skedDateTimeUtils.getDate(tempDate, timezone);
       		if (setInputDates.contains(checkDate)) {
       			skedEwtModels.DateSlot daySlot = new skedEwtModels.DateSlot(tempDate, timezone);
       			if (map_weekday_timeslots.containsKey(daySlot.weekDay)) {
       				List<skedModels.Slot> timeslots = map_weekday_timeslots.get(daySlot.weekDay);
       				if (!data.selectedWeekDays.isEmpty()) {
       					Set<String> setDaysOfWeek = new Set<String>(data.selectedWeekDays);
       					if (setDaysOfWeek.contains(daySlot.weekDay)) {
       						daySlot = checkTimeSlotOfDateSlot(timeslots, data.selectedTimeSlots, timezone,
       													tempdate, daySlot.dayString, daySlot);
       						dateslots.add(daySlot);
       					}
       				}
       				else {
       					daySlot = checkTimeSlotOfDateSlot(timeslots, data.selectedTimeSlots, timezone,
       													tempdate, daySlot.dayString, daySlot);
       					dateslots.add(daySlot);
       				}

       			}
       		}

       		tempDate = skedDateTimeUtils.addDays(tempDate, 1, timezone);
       	}


		return dateslots;
	}
    /*
	private static skedEwtModels.DateSlot checkTimeSlotOfDateSlot(List<skedModels.Slot> lstTimeSlots,
																	List<string> timeOfDate, string timezone, DateTime tempTime,
																	Map<id, skedResourceAvailabilityBase.resourceModel> mapResource,
																	string dateString, skedEwtModels.DateSlot daySlot, Location jobLocation) {
    */
    private static skedEwtModels.DateSlot checkTimeSlotOfDateSlot(List<skedModels.Slot> lstTimeSlots,
                                                                    List<string> timeOfDate, string timezone, DateTime tempTime,
                                                                    string dateString, skedEwtModels.DateSlot daySlot) {
		Integer velocity = skedSetting.instance.Admin.velocity;
		boolean ignoreTravelTimeFirstJob = skedSetting.instance.Admin.ignoreTravelTimeFirstJob;
		for (skedModels.Slot defaultSlot : lstTimeSlots) {
			if (!timeOfDate.isEmpty()) {
				Set<String> setTimeSlotIds = new Set<String>(timeOfDate);
				if (setTimeSlotIds.contains(defaultSlot.id)) {
                    /*
					skedModels.Slot slot = checkAvaiResOfTimeSlot(defaultSlot, timezone,
												mapResource, dateString, tempTime, velocity, ignoreTravelTimeFirstJob, jobLocation);
					if (slot.isAvai) {
						skedModels.Slot slot = checkQuantityOfJobEachSlot(slot, tempTime, timezone);

					}
                    */
                    skedModels.Slot slot = checkQuantityOfJobEachSlot(defaultSlot, tempTime, timezone);
					daySlot.timeslots.add(slot);
				}
			}
			else {
                /*
				skedModels.Slot slot = checkAvaiResOfTimeSlot(defaultSlot, timezone,
												mapResource, dateString, tempTime, velocity, ignoreTravelTimeFirstJob, jobLocation);
				if (slot.isAvai) {
					slot = checkQuantityOfJobEachSlot(slot, tempTime, timezone);
				}
                */
                skedModels.Slot slot = checkQuantityOfJobEachSlot(defaultSlot, tempTime, timezone);
				daySlot.timeslots.add(slot);
			}
		}

		return daySlot;
	}
    /*
	private static skedModels.Slot checkAvaiResOfTimeSlot(skedModels.Slot defaultSlot, string timezone,
																Map<id, skedResourceAvailabilityBase.resourceModel> mapResource,
																string dateString, DateTime tempTime, Integer velocity,
																boolean ignoreTravelTimeFirstJob, Location jobLocation) {
		skedModels.Slot slot = defaultSlot.deepclone();
		DateTime slotStart = skedDateTimeUtils.addMinutes(tempTime, slot.startTime, timezone);
		DateTime slotEnd = skedDateTimeUtils.addMinutes(tempTime, slot.endTime, timezone);
		//System.debug('slot.startTime = ' + slot.startTime);
		DateTime currentTime = System.now();

		if (slotStart < currentTime) {
			slot.isAvai = false;
			return slot;
		}

		//System.debug('mapResource ' + mapResource.size());
		//check resource to get available resource of each timeslot
		for (skedResourceAvailabilityBase.resourceModel res : mapResource.values()) {
			boolean isAvai = true;
			//System.debug('check res ' + res.name + ' res.mapDateSlot ' + JSON.serialize(res.mapDateslot.keySet()));
			if (res.mapDateslot.containsKey(dateString)) {
				skedResourceAvailabilityBase.dateslotModel daySlotModel =  res.mapDateslot.get(dateString);

				//ignore travel time of first job of day
				if (ignoreTravelTimeFirstJob) {
					if (daySlotModel.firstJobOfDay == null || (daySlotModel.firstJobOfDay != null &&daySlotModel.firstJobOfDay.start < slotStart)) {
						res.ignoreTravelTime = true;
					}
					else if (daySlotModel.firstJobOfDay != null &&daySlotModel.firstJobOfDay.start > slotStart){
						res.ignoreTravelTime = false;
					}
				}

				for (skedModels.Event event : daySlotModel.events) {
					if (event.start < slotEnd && event.finish > slotStart) {
				//		System.debug('remove resource ' + res.name);
						isAvai = false;
					}
				}

				//check travel time of resource
				if (isAvai) {
					isAvai = checkTravelTimeOfResourceStraightLine(jobLocation, slotStart, slotEnd, daySlotModel.events,
																res, velocity);
				//	System.debug('check travel time of res ' + res.name + ', result ' + isAvai);
				}

				if (isAvai) {
					slot.resources.add(res);
				}
			}
		}

		if (!slot.resources.isEmpty()) {
			slot.isAvai = true;
		}

		return slot;
	}

	private static boolean checkTravelTimeOfResourceStraightLine(Location jobLocation, DateTime slotStart,
																DateTime slotEnd, List<skedModels.event> events,
																skedResourceAvailabilityBase.resourceModel res,
																Integer velocity) {
		boolean isAvai = true;
		if (res.allowMoreJob || res.ignoreTravelTime) {
			return true;
		}
		//System.debug('0 slotStart = ' + slotStart);
		skedModels.event previousEvent, nextEvent;
		for (skedModels.event event : events) {
			if (slotEnd < event.start && nextEvent != NULL) {
                continue;
            }
            if (event.start < slotEnd && event.finish > slotStart) {
            	isAvai = false;
                break;
            }

            if (event.finish <= slotStart) {
            	previousEvent = event;
            	nextEvent.geoLocation = res.geoLocation;
        	}
            if (event.start >= slotEnd) {
                nextEvent = event;
                nextEvent.geoLocation = res.geoLocation;
            }
        }

        if (isAvai) {
        	if (previousEvent != null && previousEvent.geoLocation == null) {
        		previousEvent.geoLocation = res.geoLocation;
        	}

        	if (previousEvent != null && previousEvent.geoLocation != null) {
        		Integer timeFromPreJobToCurrent = skedDateTimeUtils.getDifferenteMinutes(previousEvent.finish, slotStart);
        		Decimal travelDistance = calculateDistance(previousEvent.geoLocation, jobLocation);
        		Decimal travelTime = ((travelDistance / velocity) * 60).intValue();

        		if (travelTime > timeFromPreJobToCurrent) {
        			isAvai = false;
        		}

        		if (travelDistance > res.maximumTravelRadius) {
        			isAvai = false;
        		}
        	}

        	if (nextEvent != null && nextEvent.geoLocation != null && isAvai) {
        		Integer timeFromCurrentJobToNextJob = skedDateTimeUtils.getDifferenteMinutes(nextEvent.start, slotStart);
        		Decimal travelDistance = calculateDistance(nextEvent.geoLocation, jobLocation);
        		Decimal travelTime = ((travelDistance / 50) * 60).intValue();
        		if (travelTime > timeFromCurrentJobToNextJob) {
        			isAvai = false;
        		}
        	}
        }
        return isAvai;
	}
    */

	private static Map<String, List<skedModels.Slot>> getListTimeSlots(DateTime start, DateTime finish, String timezone, String jobType) {
		Map<String, List<skedModels.Slot>> map_weekday_timeslots = new Map<String, List<skedModels.Slot>>();
		List<sked__Slot__c> skedSlots;
		skedEwtModels.SlotFilter filter = new skedEwtModels.SlotFilter(start, finish, timezone, jobType);
		System.debug('filter ' + filter);
		skedSlots = skedCommonService.getSlotInformation(filter);

        skedAutoMapper autoMap = skedAutoMapper.instance;
        Type slotDomainObjectNameType = Type.forName('skedModels.slot');
        Type jobDomainObjectNameType = Type.forName('skedModels.job');

        Date startDate = skedDateTimeUtils.getDate(start, timezone);
        Date endDate = skedDateTimeUtils.getDate(finish, timezone);

        for (sked__Slot__c skedSlot : skedSlots) {
            // check slot weekDay is in slot active time and in request period time
            Boolean weekDayFound = false;
            Date tempStartDate = skedSlot.sked_Slot_Active_Start_Date__c > startDate ? skedSlot.sked_Slot_Active_Start_Date__c : startDate;
            Date tempEndDate = skedSlot.sked_Slot_Active_End_Date__c > endDate ? endDate : skedSlot.sked_Slot_Active_End_Date__c;

            DateTime tempStartTime = skedDateTimeUtils.getStartOfDate(tempStartDate, timezone);
            DateTime tempEndTime = skedDateTimeUtils.getEndOfDate(tempEndDate, timezone).addSeconds(-5);
            DateTime tmpTime = tempStartTime;
            while (tmpTime < tempEndTime) {
                if (tmpTime.format(skedConstants.WEEKDAY, timezone).equalsIgnoreCase(skedSlot.sked_Day__c)) {
                    weekDayFound = true;
                    break;
                }
                tmpTime = skedDateTimeUtils.addDays(tmpTime, 1, timezone);
            }

            if (weekDayFound) {
            	skedModels.Slot slot = (skedModels.Slot)autoMap.mapTo(skedSlot, slotDomainObjectNameType);
            	slot = initSlot(slot);

            	if (skedSlot.sked_Jobs__r != null) {
            		for (sked__Job__c skedJob : skedSlot.sked_Jobs__r) {
            			skedModels.job job = new skedModels.job(skedJob);

            			slot.jobs.add(job);
            		}
            	}
            	List<skedModels.Slot> slots = map_weekday_timeslots.get(slot.weekDay);
            	slots = slots == null ? new List<skedModels.Slot>() : slots;
            	slots.add(slot);
            	map_weekday_timeslots.put(slot.weekDay, slots);
            }
        }

		return map_weekday_timeslots;
	}

	private static skedModels.Slot initSlot (skedModels.Slot timeslot) {
		timeslot.jobs = new List<skedModels.job>();
    	timeslot.resources = new List<skedModels.resource>();
    	timeslot.startTime = timeslot.startTime != null ? skedDateTimeUtils.convertTimeNumberToMinutes(timeslot.startTime) : timeslot.startTime;
    	timeslot.endTime = timeslot.endTime != null ? skedDateTimeUtils.convertTimeNumberToMinutes(timeslot.endTime) : timeslot.endTime;

    	return timeslot;
	}

	public static DateTime getStartOfWeek(Date selectedDate, String timezone) {
		DateTime startOfWeek = skedDateTimeUtils.getStartOfDate(selectedDate.toStartOfWeek(), timezone);
		String weekDay = startOfWeek.format(skedConstants.WEEKDAY, timezone);

		if (weekDay.equalsIgnoreCase('Sun')) {
			startOfWeek = skedDateTimeUtils.addDays(startOfWeek, 1, timezone);
		}

		return startOfWeek;
	}
    /*
	private static decimal calculateDistance(Location origin, Location target) {
		return Location.getDistance(origin, target, skedSetting.instance.Admin.distanceMeasurementUnit);
	}
    */

    private static void createAccount(skedEwtModels.BookingGridData data) {
        Account acc;
        for (skedEwtModels.ContactPrimaryDetail contactDetail : data.contactPrimaryDetails) {
            if (contactDetail.isPrimaryContact) {
                acc = new Account(
                    Name = contactDetail.name,
                    BillingStreet = data.contactInfo.address.fullAddress,
                    BillingLatitude = data.contactInfo.address.geometry.lat,
                    BillingLongitude = data.contactInfo.address.geometry.lng,
                    sked_Source__c = data.selectedSource,
                    sked_Source_Detail__c = data.selectedSourceDetail,
                    Phone = contactDetail.phone,
                    sked_Is_Cell_Phone__c = contactDetail.isCellPhone
                );
				break;
			}
		}

		if (acc != null) {
			List<Account> accounts = [SELECT Id, Name
									FROM Account
									WHERE Name = :acc.Name
									AND BillingStreet =:acc.BillingStreet];

			if (accounts == null || accounts.isEmpty()) {
				insert acc;
				data.accountId = acc.id;
				data.accountName = acc.name;
			}
			else {
				data.accountId = accounts.get(0).id;
				data.accountName = accounts.get(0).name;
			}
		}
	}

	private static void createContact(skedEwtModels.BookingGridData data) {
		Set<Contact> newContacts = new Set<Contact>();
		Contact primaryContact = new Contact();

		Set<String> contactLastNames = new Set<String>();
		Set<String> contactPhones = new Set<String>();
		Set<String> contactEmails = new Set<String>();

		for (skedEwtModels.ContactPrimaryDetail contactDetail : data.contactPrimaryDetails) {
			contactLastNames.add(contactDetail.name);
			contactPhones.add(contactDetail.phone);
			contactEmails.add(contactDetail.email);

			Contact cont = new Contact (
				LastName = contactDetail.name,
				Phone = contactDetail.phone,
                sked_Is_Cell_Phone__c = contactDetail.isCellPhone,
				MailingStreet = data.contactInfo.address.fullAddress,
				AccountId = data.accountId,
				email = contactDetail.email,
				Description = data.contactInfo.description,
				sked_Primary_Contact__c = contactDetail.isPrimaryContact
			);

			if (contactDetail.isPrimaryContact) {
				primaryContact = cont;
			}

			newContacts.add(cont);
		}

		List<Contact> contacts = [SELECT id, LastName, Phone, MailingStreet, AccountID, Email, FirstName
									FROM Contact
									WHERE (LastName IN :contactLastNames
									OR Email IN :contactEmails
									OR Phone IN :contactPhones)
									OR AccountId =:data.accountId];
		newContacts = checkDuplicatedContact(contacts, newContacts, primaryContact);
		if (!newContacts.isEmpty()) {
			insert new List<Contact>(newContacts);
		}

		data.contactId = primaryContact.id;
	}

	private static Set<Contact> checkDuplicatedContact(List<Contact> contacts,
													Set<Contact> newContacts, Contact primaryContact) {
		Set<Contact> tempContacts = new Set<Contact>(newContacts);

		if (contacts != null && !contacts.isEmpty()) {
			for (Contact existing : contacts) {
				for (Contact newCT: tempContacts) {
					if ((newCT.LastName == existing.LastName && newCT.accountId == existing.accountID) ||
						(newCT.LastName == existing.LastName && newCT.email == existing.email) ||
						(newCT.LastName == existing.LastName && newCT.phone == existing.phone && newCT.accountId == existing.accountID) ||
						(newCT.LastName == existing.LastName && newCT.MailingStreet == existing.MailingStreet) ||
						(newCT.LastName == existing.LastName && newCT.Phone == existing.Phone) ) {
						newContacts.remove(newCT);
						if (newCT.sked_Primary_Contact__c) {
							primaryContact.id = existing.id;
						}
					}
				}
			}
		}

		return newContacts;
	}

    private static List<sked__Job_Product__c> createJobProduct (skedEwtModels.BookingGridData data) {
        List<sked__Job_Product__c> jobProducts =  new List<sked__Job_Product__c>();
        if (data.products != null) {
            for (skedModels.product prod : data.products) {
                sked__Job_Product__c jobProd = new sked__Job_Product__c(
                    sked__Job__c = data.jobId,
                    sked__Product__c = prod.id
                );
                jobProducts.add(jobProd);
            }
            if (!jobProducts.isEmpty()) {
                insert jobProducts;
            }
        }
        return jobProducts;
    }

	private static sked__Job__c createJob(skedEwtModels.BookingGridData data) {
		data = updateRegionAndTimezone(data);
        Integer defaultDuration = 0;
        if (!skedSetting.instance.Admin.map_jobtype_defaultDuration.isEmpty()
            && skedSetting.instance.Admin.map_jobtype_defaultDuration.containsKey(data.jobType)) {
            defaultDuration = Integer.valueOf(skedSetting.instance.Admin.map_jobtype_defaultDuration.get(data.jobType));
        }

		sked__Job__c skedJob = new sked__Job__c(
			sked__Region__c = data.selectedRegion.id,
			sked__Address__c = data.contactInfo.address.fullAddress,
			sked__GeoLocation__latitude__s = data.contactInfo.address.geometry.lat,
			sked__GeoLocation__longitude__s = data.contactInfo.address.geometry.lng,
			sked__Job_Status__c = skedConstants.JOB_STATUS_QUEUED,
			sked__Account__c = data.accountId,
			sked__Contact__c = data.contactId,
			sked__Duration__c = defaultDuration != 0 ? defaultDuration : 60,
			sked__Description__c = data.contactInfo.description,
			sked__type__c = data.jobType,
			sked__Notes_Comments__c = data.contactInfo.description
		);

		if (skedJob.sked__type__c == skedConstants.JOB_TYPE_CONSULTATION) {
			skedJob.sked__Description__c = data.selectedSource + ' - ' + data.accountName;
		}
        else {
            if (data.products != null && !data.products.isEmpty()) {
                skedJob.sked__Description__c = data.products[0].productType + ' - ' + data.accountName;
            }
        }

        if (skedConstants.JOB_TYPE_INSTALLATION_WH.equalsIgnoreCase(skedJob.sked__type__c)
            || skedConstants.JOB_TYPE_INSTALLATION_RO.equalsIgnoreCase(skedJob.sked__type__c)) {
            skedJob.sked_Dig_Tess_Required__c = data.digTess;
            skedJob.sked_Site_Survey_Required__c = data.siteSurvey;
            skedJob.sked_1_25_Main__c = data.main;
            skedJob.sked_40_ft_Trench__c = data.trench;
            skedJob.sked_Garage_Manifold__c = data.garageManifold;
            skedJob.sked_Granite_Drill__c = data.graniteDrill;
            skedJob.sked_Exterior_Manifold__c = data.exteriorManifold;
            skedJob.sked_Permit__c = data.permit;
            //EWT-149
            skedJob.sked_Shed_Exterior_Placement__c = data.shedExteriorPlacement;
            skedJob.sked_Driveway_Bore__c = data.driveBore;
            skedJob.sked_Ice_Line__c = data.iceLine;
            skedJob.sked_InstallSpecialAttribute1__c = data.installSpecialAttribute1;
            skedJob.sked_InstallSpecialAttribute2__c = data.installSpecialAttribute2;
        }
        if (skedConstants.JOB_TYPE_SERVICE.equalsIgnoreCase(skedJob.sked__type__c)) {
            skedJob.sked__Urgency__c = data.urgencyType;
        }

		if (data.jobStarTime != null && data.jobEndTime != null) {
			String timezone = data.selectedRegion.timezoneSidId;
			Date selectedDate = Date.valueOf(data.selectedDate);
			DateTime jobDate = DateTime.newInstance(selectedDate, Time.newInstance(0, 0, 0, 0));
			jobDate = skedDateTimeUtils.switchTimezone(jobDate,UserInfo.getTimeZone().getId(), timezone);
			integer startMinutes = data.jobStarTime;
			integer finishMinutes = data.jobEndTime;
			DateTime jobStart = skedDateTimeUtils.addMinutes(jobDate, startMinutes, timezone);
			DateTime jobFinish = skedDateTimeUtils.addMinutes(jobDate, finishMinutes, timezone);
			integer jobDuration = finishMinutes - startMinutes;
			skedJob.sked__Start__c = jobStart;
			skedJob.sked__Finish__c = jobFinish;
			skedJob.sked__Duration__c = jobDuration;
			skedJob.sked__Job_Status__c = skedConstants.JOB_STATUS_PENDING_ALLOCATION;
			skedJob.sked_Slot__c = data.selectedSlot.id;
		}

		insert skedJob;

		data.jobId = skedJob.id;

		return skedJob;
	}

	private static string createJobTag(skedEwtModels.BookingGridData data) {
        String tagName = data.selectedSource;
        List<sked__Job_Tag__c> skedJobTags = new List<sked__Job_Tag__c>();

        if (String.isBlank(tagName) && skedConstants.JOB_TYPE_CONSULTATION.equalsIgnoreCase(data.jobType)) {
            return skedConstants.MISSING_SOURCE;
        }

        List<sked__Tag__c> skedTags =  new List<sked__Tag__c>();
        if (skedConstants.JOB_TYPE_CONSULTATION.equalsIgnoreCase(data.jobType)) {
            tagName = '%' + tagName.replace(' ', '%') + '%';
            skedTags = [SELECT id, Name FROM sked__Tag__c WHERE Name LIKE :tagName];
        }
        else if ( (skedConstants.JOB_TYPE_INSTALLATION_RO.equalsIgnoreCase(data.jobType)
                || skedConstants.JOB_TYPE_SERVICE.equalsIgnoreCase(data.jobType)
                || skedConstants.JOB_TYPE_INSTALLATION_WH.equalsIgnoreCase(data.jobType) )
                && data.products != null && !data.products.isEmpty()) {
            tagName = data.products[0].productType;
            skedTags = [SELECT id, Name FROM sked__Tag__c WHERE Name = :tagName];
        }

        //if ((skedTags == null || skedTags.isEmpty()) && skedConstants.JOB_TYPE_CONSULTATION.equalsIgnoreCase(data.jobType)){
        if (skedTags == null || skedTags.isEmpty()){
            return skedConstants.MISSING_TAG;
        }

        for (sked__Tag__c skedTag : skedTags) {
            sked__Job_Tag__c skedJobTag = new sked__Job_Tag__c(
                sked__Job__c = data.jobId,
                sked__Tag__c = skedTag.id
            );
            skedJobTags.add(skedJobTag);
        }

        if (!skedJobTags.isEmpty()) {
            insert skedJobTags;
        }

        return '';
    }

	private static skedModels.Slot checkQuantityOfJobEachSlot(skedModels.Slot slot, DateTime tempTime, String timezone) {
        DateTime currentTime = System.now();
        DateTime slotStart = skedDateTimeUtils.addMinutes(tempTime, slot.startTime, timezone);
        DateTime slotEnd = skedDateTimeUtils.addMinutes(tempTime, slot.endTime, timezone);
        if (slotStart < currentTime || slot.quantity == null || slot.quantity == 0) {
            slot.isAvai = false;
            return slot;
        }
        slot.isAvai = true;
		Integer noOfJob = 0;
		System.debug('slot ' + slot.name + ', slot.jobs ' + slot.jobs.size());
		if (slot.jobs != null && !slot.jobs.isEmpty()) {
			for (skedModels.job job : slot.jobs) {
				System.debug('job ' + job);
				if (job.start < slotEnd && job.finish > slotStart) {
					noOfJob++;
				}
				System.debug('noOfJob ' + noOfJob);
			}

			if (noOfJob >= slot.quantity) {
				slot.isAvai = false;
			}
		}
		return slot;
	}


}