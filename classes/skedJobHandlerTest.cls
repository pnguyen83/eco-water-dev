@isTest
public class skedJobHandlerTest {
    @isTest static void test_JobChangeTime() {
        Map<string, sObject> mapObject = skedDataSetup.setupCommonTestData();
        skedEwtModels.BookingGridData data = skedBookingHandlerTest.buildApointmentData(skedConstants.JOB_TYPE_CONSULTATION, mapObject);
        String timezone = UserInfo.getTimeZone().getId();

        Test.startTest();
        skedRemoteResultModel result = skedBookingController.createAppointment(data);
        sked__Job__c job = (sked__Job__c)result.data;
        job.sked__Start__c = skedDateTimeUtils.addMinutes(job.sked__Start__c, 30, timezone);
        job.sked__Finish__c = skedDateTimeUtils.addMinutes(job.sked__Finish__c, -30, timezone);
        job.sked__Duration__c = skedDateTimeUtils.getDifferenteMinutes(job.sked__Start__c, job.sked__Finish__c);
        update job;

        skedEwtModels.BookingGridData installationData = skedBookingHandlerTest.buildApointmentData(skedConstants.JOB_TYPE_INSTALLATION_WH, mapObject);
        skedRemoteResultModel instalationResult = skedBookingController.createAppointment(installationData);
        sked__Job__c instalationJob = (sked__Job__c)instalationResult.data;
        instalationJob.sked__Start__c = skedDateTimeUtils.addMinutes(instalationJob.sked__Start__c, 30, timezone);
        instalationJob.sked__Finish__c = skedDateTimeUtils.addMinutes(instalationJob.sked__Finish__c, -30, timezone);
        instalationJob.sked__Duration__c = skedDateTimeUtils.getDifferenteMinutes(instalationJob.sked__Start__c, instalationJob.sked__Finish__c);
        update instalationJob;
        Test.stopTest();
    }
}