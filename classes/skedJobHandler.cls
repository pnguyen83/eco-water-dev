public with sharing class skedJobHandler {
	public static void onAfterInsert(List<sked__Job__c> newSkedJobs) {
		try {
			sendCustomerNotificationEmail(newSkedJobs, null);
		}
		catch (Exception ex) {
			System.debug('error: ' + ex.getMessage() + ', tracing: ' + ex.getStackTraceString());
		}
	}

	public static void onAfterUpdate(List<sked__Job__c> newSkedJobs, Map<id, sked__Job__c> map_old_jobs) {
		sendCustomerNotificationEmail(newSkedJobs, map_old_jobs);
	}

	//=============================================================Private Functions===========================================//
	private static void sendCustomerNotificationEmail (List<sked__Job__c> newSkedJobs, Map<id, sked__Job__c> map_old_jobs) {
		Map<Id, sked__Job__c> map_consultation_jobs = new Map<Id, sked__Job__c>();
		Map<Id, sked__Job__c> map_installation_jobs = new Map<Id, sked__Job__c>();
		List<Messaging.Singleemailmessage> emails = new List<Messaging.Singleemailmessage>();

		for (sked__Job__c skedJob : newSkedJobs) {
			System.debug('skedJob ' + skedJob);
			if (skedJob.sked__Start__c != null) {
				if (map_old_jobs != null) {
					System.debug('1');
					if (map_old_jobs.containsKey(skedJob.id)) {
						System.debug('2');
						sked__Job__c oldJob = map_old_jobs.get(skedJob.id);
						if (skedJob.sked__Start__c != oldJob.sked__Start__c || skedJob.sked__Finish__c != oldJob.sked__Finish__c) {
							System.debug('3');
							if (skedJob.sked__Type__c.equalsIgnoreCase(skedConstants.JOB_TYPE_CONSULTATION)) {
								System.debug('4');
								map_consultation_jobs.put(skedJob.id, skedJob);
							}
							else if (skedJob.sked__Type__c.equalsIgnoreCase(skedConstants.JOB_TYPE_INSTALLATION_WH)
								|| skedJob.sked__Type__c.equalsIgnoreCase(skedConstants.JOB_TYPE_INSTALLATION_RO)) {
								System.debug('5');
								map_installation_jobs.put(skedJob.id, skedJob);
							}
						}
					}

				}
				else {
					System.debug('6');
					if (skedJob.sked__Type__c.equalsIgnoreCase(skedConstants.JOB_TYPE_CONSULTATION)) {
						System.debug('7');
						map_consultation_jobs.put(skedJob.id, skedJob);
					}
					else if (skedJob.sked__Type__c.equalsIgnoreCase(skedConstants.JOB_TYPE_INSTALLATION_WH)
						|| skedJob.sked__Type__c.equalsIgnoreCase(skedConstants.JOB_TYPE_INSTALLATION_RO)) {
						System.debug('8');
						map_installation_jobs.put(skedJob.id, skedJob);
					}
				}
			}

		}

		if (!map_consultation_jobs.isEmpty()) {
			System.debug('9');
			emails = createNotificationEmail(map_consultation_jobs, skedConstants.EMAIL_CONSULTATION_TEMPLATE,
																			skedConstants.EMAIL_CONSULTATION_ADDRESS, emails);
		}

		if (!map_installation_jobs.isEmpty()) {
			System.debug('10');
			emails = createNotificationEmail(map_installation_jobs, skedConstants.EMAIL_INSTALLATION_TEMPLATE,
																			skedConstants.EMAIL_INSTALLATION_ADDRESS, emails);
		}
		System.debug('emails ' + emails);
		if (!emails.isEmpty()) {
			Messaging.sendEmail(emails);
		}
	}

	private static List<Messaging.Singleemailmessage> createNotificationEmail(Map<id, sked__Job__c> map_job, String emailTempateName, String replyToAddress,
												List<Messaging.Singleemailmessage> emails) {
		skedConfigs__c config = skedConfigs__c.getOrgDefaults();
		String admissionEmail = '';
		if (skedConstants.EMAIL_CONSULTATION_TEMPLATE.equalsIgnoreCase(emailTempateName)) {
			admissionEmail = 'marketing@ecowatertexas.com';
			if (config != null && String.isNotBlank(config.Marketing_Email__c)) {
				admissionEmail = config.Marketing_Email__c;
			}
		}
		else if (skedConstants.EMAIL_INSTALLATION_TEMPLATE.equalsIgnoreCase(emailTempateName)) {
			admissionEmail = 'kris@ecowatertexas.com';
			if (config != null && String.isNotBlank(config.Sales_Email__c)) {
				admissionEmail = config.Sales_Email__c;
			}
		}
		OrgWideEmailAddress orgWideEmail = skedCommonService.getOrgWideEmail(admissionEmail);

		for (sked__Job__c skedJob : map_job.values()) {
			System.debug('1 skedJob ' + skedJob);
			String jobDateTime = '';
			if (skedJob.sked__Start__c != null && skedJob.sked_Contact_Email__c != null) {
				jobDateTime = skedJob.sked__Start__c.format(skedConstants.WEEKDAY_DAY_MONTH_YEAR, skedJob.sked__Timezone__c);

				Messaging.Singleemailmessage email = skedCommonService.createEmail(skedJob.sked_Account_Name__c, skedJob.sked_Contact_Email__c,
																emailTempateName, jobDateTime, replyToAddress, orgWideEmail);
				emails.add(email);
			}
		}

		return emails;
	}
}