public class skedQueryBuilder {

    public string fromClause;
    public Set<string> fields;
    public List<string> conditions;
    public List<string> extraClauses;

    public skedQueryBuilder() {
        this.fields = new Set<string>();
        this.conditions = new List<string>();
        this.extraClauses = new List<string>();
    }

    //syntax: condition = 'Id = {0}'; value = '123456789123456789'
    public string generateCondition(string condition, string value) {
        string param = '\'' + value + '\'';
        condition = condition.replace('{0}', param);
        return condition;
    }

    //syntax: condition = 'sked__Start__c >= {0}'; value = 2018-07-30T08:00:00.000+0000
    public string generateCondition(string condition, DateTime value) {
        string param = Json.serialize(value).replace('"', '');
        condition = condition.replace('{0}', param);
        return condition;
    }

    //syntax: condition = 'ID IN {0}', values = {'123456789123456789','123456789123456789'}
    public string generateCondition(string condition, List<string> values) {
        List<string> params = new List<string>();
        for (string value : values) {
            params.add('\'' + value + '\'');
        }
        condition = condition.replace('{0}', '(' + string.join(params, ',') + ')');
        return condition;
    }

    //syntax: condition = 'ID IN {0}', values = {'123456789123456789','123456789123456789'}
    public string generateCondition(string condition, Set<Id> values) {
        List<string> params = new List<string>();
        for (string value : values) {
            params.add('\'' + value + '\'');
        }
        condition = condition.replace('{0}', '(' + string.join(params, ',') + ')');
        return condition;
    }

    //syntax: condition = 'Id = {0}'; value = '123456789123456789'
    public void addCondition(string condition, string value) {
        this.conditions.add(generateCondition(condition, value));
    }

    //syntax: condition = 'sked__Start__c >= {0}'; value = 2018-07-30T08:00:00.000+0000
    public void addCondition(string condition, DateTime value) {
        this.conditions.add(generateCondition(condition, value));
    }

    //syntax: condition = 'ID IN {0}', values = {'123456789123456789','123456789123456789'}
    public void addCondition(string condition, List<string> values) {
        this.conditions.add(generateCondition(condition, values));
    }

    //syntax: condition = 'ID IN {0}', values = {'123456789123456789','123456789123456789'}
    public void addCondition(string condition, Set<Id> values) {
        this.conditions.add(generateCondition(condition, values));
    }

    public void addCondition(string condition) {
        this.conditions.add(condition);
    }

    public void addConditions(List<string> conditions) {
        this.conditions.addAll(conditions);
    }

    public void addField(string fieldName) {
        this.fields.add(fieldName);
    }

    public void addFields(List<string> fieldNames) {
        this.fields.addAll(fieldNames);
    }

    public void addExtraClauses(string extraClause) {
        this.extraClauses.add(extraClause);
    }

    public void addExtraClauses(List<string> extraClauses) {
        this.extraClauses.addAll(extraClauses);
    }

    public void setFromClause(string fromClause) {
        this.fromClause = fromClause;
    }

    public override string toString() {
        string statement = 'SELECT {0} FROM {1}';
        List<String> fieldList = new List<string>();
        fieldList.addAll(this.fields);
        statement = statement.replace('{0}', string.join(fieldList, ', '));
        statement = statement.replace('{1}', this.fromClause);
        if (!this.conditions.isEmpty()) {
            statement += ' WHERE ' + string.join(this.conditions, ' AND ');
        }
        if (!this.extraClauses.isEmpty()) {
            statement += ' ' + string.join(this.extraClauses, ' ');
        }
        return statement;
    }

}