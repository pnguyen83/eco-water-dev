@isTest
public class skedLocationServicesTest {

    static testmethod void testLocationService() {
        String TEST_DATA = '{"results":[{"address_components":[{"long_name":"StationStreet","short_name":"StationSt","types":["route"]},{"long_name":"Sunbury","short_name":"Sunbury","types":["locality","political"]},{"long_name":"HumeCity","short_name":"Hume","types":["administrative_area_level_2","political"]},{"long_name":"Victoria","short_name":"VIC","types":["administrative_area_level_1","political"]},{"long_name":"Australia","short_name":"AU","types":["country","political"]},{"long_name":"3429","short_name":"3429","types":["postal_code"]}],"formatted_address":"StationSt&EvansSt,SunburyVIC3429,Australia","geometry":{"location":{"lat":-37.58204540000001,"lng":144.7276305},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":-37.58069641970851,"lng":144.7289794802915},"southwest":{"lat":-37.58339438029151,"lng":144.7262815197085}}},"place_id":"EjJTdGF0aW9uIFN0ICYgRXZhbnMgU3QsIFN1bmJ1cnkgVklDIDM0MjksIEF1c3RyYWxpYQ","types":["intersection"]}],"status":"OK"}';

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(TEST_DATA));
        sked__Resource__c res = new sked__Resource__c(Name='testResource', sked__Home_Address__c='test address');
        insert res;

        skedLocationServices.updateGeoLocation(res.Id, 'sked__Resource__c');

        //Test batch
        skedBatchProcessor batch = new skedBatchProcessor(res.Id, skedLocationServices.OBJ_RESOURCE);
        Database.executeBatch(batch,1);

        Test.stopTest();
    }
}