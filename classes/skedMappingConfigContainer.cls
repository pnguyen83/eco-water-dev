public class skedMappingConfigContainer {

    public static final string CUSTOM_FIELDS_EXTENSION = '__c';

    /************************************************************* Singleton stuff *************************************************************/
    private static skedMappingConfigContainer mInstance;

    public static skedMappingConfigContainer instance {
        get {
            if (mInstance == null) {
                mInstance = new skedMappingConfigContainer();
            }
            return mInstance;
        }
    }

    private skedMappingConfigContainer() {
        this.mapConfig = new Map<string, mappingConfigModel>();
    }

    /************************************************************* AutoMapperconfigContainer *************************************************************/
    protected Map<string, mappingConfigModel> mapConfig;

    public mappingConfigModel getMappingConfig(string sObjectType) {
        mappingConfigModel mappingConfig;
        if (!this.mapConfig.containsKey(sObjectType)) {
            //TODO: need to remove hard-code on the below code for handling class name
            string className = sObjectType.removeEnd(CUSTOM_FIELDS_EXTENSION);
            className = className.startsWith('sked_') ? className.remove('sked_') : className;
            className = className.remove('_');
            className = 'skedMappingConfigContainer.' + className + 'MappingConfigFactory';
            Type mappingConfigFactoryType = Type.forName(className);

            mappingConfigFactory factoryInstance = (mappingConfigFactory)mappingConfigFactoryType.newInstance();
            mappingConfig = factoryInstance.process();
            this.mapConfig.put(sObjectType, mappingConfig);
        }
        else {
            mappingConfig = this.mapConfig.get(sObjectType);
        }
        return mappingConfig;
    }

    /************************************************************* Nested class *************************************************************/
    public class mappingConfigModel {
        public List<fieldConfigModel> fieldConfigs;
        public Set<string> readonlyFields;
        public Set<string> masterDetailFields;

        public mappingConfigModel() {
            this.fieldConfigs = new List<fieldConfigModel>();
        }

        public fieldConfigModel addFieldConfig(string sObjectFieldPath, string domainFieldName, string mappingType) {
            fieldConfigModel fieldConfig = new fieldConfigModel();
            fieldConfig.sObjectFieldPath = sObjectFieldPath;
            fieldConfig.sObjectFieldPaths = sObjectFieldPath.split('\\.');
            fieldConfig.domainFieldName = domainFieldName;
            fieldConfig.mappingType = mappingType;
            this.fieldConfigs.add(fieldConfig);
            return fieldConfig;
        }
        /*
        public fieldConfigModel addFieldConfig(string sObjectFieldPath, string domainFieldName, string mappingType, Type relatedObjectDomainType, string relatedSObjectName) {
            fieldConfigModel fieldConfig = new fieldConfigModel();
            fieldConfig.sObjectFieldPath = sObjectFieldPath;
            fieldConfig.sObjectFieldPaths = sObjectFieldPath.split('\\.');
            fieldConfig.domainFieldName = domainFieldName;
            fieldConfig.mappingType = mappingType;
            fieldConfig.relatedObjectDomainType = relatedObjectDomainType;
            fieldConfig.relatedSObjectName = relatedSObjectName;
            this.fieldConfigs.add(fieldConfig);
            return fieldConfig;
        }

        public fieldConfigModel addFieldConfig(string sObjectFieldPath, string domainFieldName, string mappingType, string relatedListSObjectName) {
            fieldConfigModel fieldConfig = new fieldConfigModel();
            fieldConfig.sObjectFieldPath = sObjectFieldPath;
            fieldConfig.sObjectFieldPaths = sObjectFieldPath.split('\\.');
            fieldConfig.domainFieldName = domainFieldName;
            fieldConfig.mappingType = mappingType;
            fieldConfig.relatedListSObjectName = relatedListSObjectName;
            this.fieldConfigs.add(fieldConfig);
            return fieldConfig;
        }
        */
    }

    public class fieldConfigModel {
        public string sObjectFieldPath;
        public List<string> sObjectFieldPaths;
        public string domainFieldName;
        public string mappingType;
        public Type relatedObjectDomainType;
        public string relatedSObjectName;
        public string relatedListSObjectName;
    }

    /************************************************************* Mapping Config Factory *************************************************************/
    public virtual class mappingConfigFactory {
        public virtual mappingConfigModel process() {
            return null;
        }
    }
    /*
    public class accountMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            mappingConfig.addFieldConfig('Phone', 'phone', 'direct');

            mappingConfig.readonlyFields = new Set<string>();
            mappingConfig.readonlyFields.add('Name');
            return mappingConfig;
        }
    }

    public class activityMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            mappingConfig.addFieldConfig('sked__Type__c', 'eventType', 'direct');
            mappingConfig.addFieldConfig('sked__Notes__c', 'notes', 'direct');
            mappingConfig.addFieldConfig('sked__Start__c', 'start', 'datetime');
            mappingConfig.addFieldConfig('sked__End__c', 'end', 'datetime');
            mappingConfig.addFieldConfig('sked__Address__c', 'address', 'fullAddress');
            mappingConfig.addFieldConfig('sked__GeoLocation__c', 'address', 'addressLocation');
            mappingConfig.addFieldConfig('sked_Recurring_Schedule__c', 'scheduleId', 'direct');
            mappingConfig.addFieldConfig('activity', 'objectType', 'value');

            mappingConfig.readonlyFields = new Set<string>();
            mappingConfig.readonlyFields.add('Name');
            return mappingConfig;
        }
    }

    public class availabilityMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            mappingConfig.addFieldConfig('sked__Status__c', 'status', 'direct');
            mappingConfig.addFieldConfig('sked__Type__c', 'eventType', 'direct');
            mappingConfig.addFieldConfig('sked__Notes__c', 'notes', 'direct');
            mappingConfig.addFieldConfig('sked__Is_Available__c', 'isAvailable', 'direct');
            mappingConfig.addFieldConfig('sked__Start__c', 'start', 'datetime');
            mappingConfig.addFieldConfig('sked__Finish__c', 'end', 'datetime');
            mappingConfig.addFieldConfig('sked_Is_All_Day__c', 'isAllDay', 'direct');
            mappingConfig.addFieldConfig('sked_Recurring_Schedule__c', 'scheduleId', 'direct');
            mappingConfig.addFieldConfig('availability', 'objectType', 'value');

            mappingConfig.readonlyFields = new Set<string>();
            mappingConfig.readonlyFields.add('Name');
            return mappingConfig;
        }
    }

    public class caseMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('CaseNumber', 'name', 'direct');

            mappingConfig.readonlyFields = new Set<string>();
            mappingConfig.readonlyFields.add('Name');
            return mappingConfig;
        }
    }

    public class jobMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            mappingConfig.addFieldConfig('sked__Account__c', 'accountId', 'direct');
            mappingConfig.addFieldConfig('sked__Account__c', 'patientId', 'direct');
            mappingConfig.addFieldConfig('sked__Account__r.Name', 'patientName', 'direct');
            mappingConfig.addFieldConfig('sked__Address__c', 'address', 'fullAddress');
            mappingConfig.addFieldConfig('sked__Contact__c', 'contactId', 'direct');
            mappingConfig.addFieldConfig('sked__Contact__r.Name', 'contactName', 'direct');
            mappingConfig.addFieldConfig('sked__Description__c', 'description', 'direct');
            mappingConfig.addFieldConfig('sked__Duration__c', 'duration', 'direct');
            mappingConfig.addFieldConfig('sked__Finish__c', 'end', 'datetime');
            mappingConfig.addFieldConfig('sked__Job_Allocation_Count__c', 'jobAllocationCount', 'direct');
            mappingConfig.addFieldConfig('sked__Job_Status__c', 'jobStatus', 'direct');
            mappingConfig.addFieldConfig('sked__GeoLocation__c', 'address', 'addressLocation');
            mappingConfig.addFieldConfig('sked__Location__c', 'serviceLocationId', 'direct');
            mappingConfig.addFieldConfig('sked__Notes_Comments__c', 'notes', 'direct');
            mappingConfig.addFieldConfig('sked__Quantity__c', 'quantity', 'direct');
            mappingConfig.addFieldConfig('sked__Recurring_Schedule__c', 'scheduleId', 'direct');
            mappingConfig.addFieldConfig('sked__Region__c', 'regionId', 'direct');
            mappingConfig.addFieldConfig('sked__Start__c', 'start', 'datetime');
            mappingConfig.addFieldConfig('sked__Type__c', 'eventType', 'direct');
            mappingConfig.addFieldConfig('sked_Address_Postal_Code__c', 'address', 'addressPostalCode');
            mappingConfig.addFieldConfig('sked_Case__c', 'caseId', 'direct');
            mappingConfig.addFieldConfig('sked_Case__r.CaseNumber', 'caseNumber', 'direct');
            mappingConfig.addFieldConfig('sked_Cancellation_Reason__c', 'cancellationReason', 'direct');
            mappingConfig.addFieldConfig('sked_Cancellation_Reason_Notes__c', 'cancellationReasonNotes', 'direct');
            mappingConfig.addFieldConfig('sked_Group_Event__c', 'groupEventId', 'direct');
            mappingConfig.addFieldConfig('sked_Group_Event__r.Name', 'groupEventName', 'direct');
            mappingConfig.addFieldConfig('sked_Is_Care_Team__c', 'isCareTeam', 'direct');
            mappingConfig.addFieldConfig('sked_Job_Attendee_Count__c', 'jobAttendeeCount', 'direct');

            mappingConfig.addFieldConfig('sked__Job_Allocations__r', 'allocations', 'relatedList', 'sked__Job_Allocation__c');
            mappingConfig.addFieldConfig('sked__JobTags__r', 'jobTags', 'relatedList', 'sked__Job_Tag__c');
            mappingConfig.addFieldConfig('sked_Job_Offers__r', 'jobOffers', 'relatedList', 'sked_Job_Offer__c');
            mappingConfig.addFieldConfig('sked_Job_Attendees__r', 'attendees', 'relatedList', 'sked_Job_Attendee__c');

            mappingConfig.addFieldConfig('job', 'objectType', 'value');

            mappingConfig.readonlyFields = new Set<string>();
            mappingConfig.readonlyFields.add('Name');
            return mappingConfig;
        }
    }

    public class jobAllocationMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            mappingConfig.addFieldConfig('sked__Job__r', 'job', 'related', skedModels.job.class, 'sked__Job__c');
            mappingConfig.addFieldConfig('sked__Resource__r', 'resource', 'related', skedModels.resource.class, 'sked__Resource__c');
            mappingConfig.addFieldConfig('jobAllocation', 'objectType', 'value');

            mappingConfig.readonlyFields = new Set<string>();
            mappingConfig.readonlyFields.add('Name');
            return mappingConfig;
        }
    }

    public class jobTagMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            mappingConfig.addFieldConfig('sked__Job__c', 'jobId', 'direct');
            mappingConfig.addFieldConfig('sked__Tag__c', 'tagId', 'direct');
            mappingConfig.addFieldConfig('sked__Tag__r', 'tag', 'related', skedModels.tag.class, 'sked__Tag__c');
            mappingConfig.addFieldConfig('jobTag', 'objectType', 'value');

            mappingConfig.readonlyFields = new Set<string>();
            mappingConfig.readonlyFields.add('Name');
            return mappingConfig;
        }
    }

    public class regionMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            mappingConfig.addFieldConfig('sked__Timezone__c', 'timezoneSidId', 'direct');

            mappingConfig.readonlyFields = new Set<string>();
            mappingConfig.readonlyFields.add('Name');
            return mappingConfig;
        }
    }

    public class resourceMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            mappingConfig.addFieldConfig('sked__Category__c', 'category', 'direct');
            mappingConfig.addFieldConfig('sked__Primary_Region__c', 'regionId', 'direct');
            mappingConfig.addFieldConfig('sked__Primary_Region__r.Name', 'regionName', 'direct');
            mappingConfig.addFieldConfig('sked__User__c', 'userId', 'direct');
            mappingConfig.addFieldConfig('sked__User__r.SmallPhotoUrl', 'photoUrl', 'direct');
            mappingConfig.addFieldConfig('sked__Home_Address__c', 'address', 'fullAddress');
            mappingConfig.addFieldConfig('sked__GeoLocation__c', 'address', 'addressLocation');

            mappingConfig.addFieldConfig('sked__Job_Allocations__r', 'allocations', 'relatedList', 'sked__Job_Allocation__c');
            mappingConfig.addFieldConfig('sked__Availabilities1__r', 'availabilities', 'relatedList', 'sked__Availability__c');
            mappingConfig.addFieldConfig('sked__Activities__r', 'activities', 'relatedList', 'sked__Activity__c');
            mappingConfig.addFieldConfig('sked__Resource_Shifts__r', 'resourceShifts', 'relatedList', 'sked__Resource_Shift__c');

            mappingConfig.readonlyFields = new Set<string>();
            return mappingConfig;
        }
    }

    public class resourceShiftMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('sked__Resource__c', 'resourceId', 'direct');
            mappingConfig.addFieldConfig('sked__Shift__c', 'shiftId', 'direct');
            mappingConfig.addFieldConfig('sked__Shift__r', 'shift', 'related', skedModels.shift.class, 'sked__Shift__c');

            mappingConfig.readonlyFields = new Set<string>();
            mappingConfig.readonlyFields.add('Name');
            return mappingConfig;
        }
    }

    public class resourceTagMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            mappingConfig.addFieldConfig('sked__Resource__c', 'resourceId', 'direct');
            mappingConfig.addFieldConfig('sked__Tag__c', 'tagId', 'direct');
            mappingConfig.addFieldConfig('sked__Expiry__c', 'expiry', 'datetime');
            mappingConfig.addFieldConfig('sked__Tag__r', 'tag', 'related', skedModels.tag.class, 'sked__Tag__c');
            mappingConfig.addFieldConfig('jobTag', 'objectType', 'value');

            mappingConfig.readonlyFields = new Set<string>();
            mappingConfig.readonlyFields.add('Name');
            return mappingConfig;
        }
    }

    public class shiftMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            mappingConfig.addFieldConfig('sked__Duration__c', 'duration', 'direct');
            mappingConfig.addFieldConfig('sked__End__c', 'end', 'datetime');
            mappingConfig.addFieldConfig('sked__Location__c', 'locationId', 'direct');
            mappingConfig.addFieldConfig('sked__Region__c', 'regionId', 'direct');
            mappingConfig.addFieldConfig('sked__Start__c', 'start', 'datetime');
            mappingConfig.addFieldConfig('sked_Address__c', 'address', 'fullAddress');
            mappingConfig.addFieldConfig('sked_Description__c', 'description', 'direct');
            mappingConfig.addFieldConfig('sked_GeoLocation__c', 'address', 'addressLocation');
            mappingConfig.addFieldConfig('sked_Type__c', 'eventType', 'direct');

            mappingConfig.addFieldConfig('sked__Resource_Shifts__r', 'resourceShifts', 'relatedList', 'sked__Resource_Shift__c');
            mappingConfig.addFieldConfig('shift', 'objectType', 'value');

            mappingConfig.readonlyFields = new Set<string>();
            mappingConfig.readonlyFields.add('Name');
            mappingConfig.readonlyFields.add('sked__End__c');
            return mappingConfig;
        }
    }

    public class tagMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            return mappingConfig;
        }
    }

    public class contactMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            mappingConfig.addFieldConfig('FirstName', 'firstName', 'direct');
            mappingConfig.addFieldConfig('LastName', 'lastName', 'direct');
            mappingConfig.addFieldConfig('Phone', 'phone', 'direct');

            mappingConfig.readonlyFields = new Set<string>();
            mappingConfig.readonlyFields.add('Name');
            return mappingConfig;
        }
    }
    */

    public class slotMappingConfigFactory extends mappingConfigFactory {
        public override mappingConfigModel process() {
            mappingConfigModel mappingConfig = new mappingConfigModel();
            mappingConfig.addFieldConfig('Id', 'id', 'direct');
            mappingConfig.addFieldConfig('Name', 'name', 'direct');
            mappingConfig.addFieldConfig('sked__Customer_White_Listed__c', 'customerWhiteList', 'direct');
            mappingConfig.addFieldConfig('sked_Day__c', 'weekday', 'direct');
            mappingConfig.addFieldConfig('sked__Job__c', 'jobId', 'direct');
            mappingConfig.addFieldConfig('sked_Job_Type__c', 'jobType', 'direct');
            mappingConfig.addFieldConfig('sked__Location_White_Listed__c', 'locationWhiteList', 'direct');
            mappingConfig.addFieldConfig('sked__Quantity__c', 'quantity', 'direct');
            mappingConfig.addFieldConfig('sked__Resource_Type__c', 'resourceType', 'direct');
            mappingConfig.addFieldConfig('sked_Slot_Active_Start_Date__c', 'activeStartDate', 'direct');
            mappingConfig.addFieldConfig('sked_Slot_Active_End_Date__c', 'activeEndDate', 'direct');
            mappingConfig.addFieldConfig('sked_Slot_Start_Time__c', 'startTime', 'direct');
            mappingConfig.addFieldConfig('sked_Slot_End_Time__c', 'endTime', 'direct');

            mappingConfig.readonlyFields = new Set<string>();

            return mappingConfig;
        }
    }

}