public class skedResourceTriggerHandler {
    public static void afterInsert(List<sObject> oldRecords, Map<Id, sObject> newMap) {
        if(!System.isFuture() && !System.isBatch()) skedLocationServices.updateLocationGeocode(oldRecords, newMap, skedLocationServices.OBJ_RESOURCE);
    }

    public static void afterUpdate(List<sObject> oldRecords, Map<Id, sObject> newMap) {
        system.debug('sked::resource.afterUpdate');
        if(!System.isFuture() && !System.isBatch()) skedLocationServices.updateLocationGeocode(oldRecords, newMap, skedLocationServices.OBJ_RESOURCE);
    }
}