trigger skedResourceTrigger on sked__Resource__c (after insert, after update) {
    boolean isAfterInsert   = trigger.isAfter && trigger.isInsert;
    boolean isAfterUpdate   = trigger.isAfter && trigger.isUpdate;

    if(isAfterInsert){
        skedResourceTriggerHandler.afterInsert(trigger.old, trigger.newMap);
    }

    if(isAfterUpdate){
        skedResourceTriggerHandler.afterUpdate(trigger.old, trigger.newMap);
    }
}