trigger skedAccountTrigger on Account (after insert, after update) {
    boolean isAfterInsert   = trigger.isAfter && trigger.isInsert;
    boolean isAfterUpdate   = trigger.isAfter && trigger.isUpdate;

    if(isAfterInsert){
        skedAccountTriggerHandler.afterInsert(trigger.old, trigger.newMap);
    }

    if(isAfterUpdate){
        skedAccountTriggerHandler.afterUpdate(trigger.old, trigger.newMap);
    }

}