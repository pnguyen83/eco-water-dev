trigger skedJobTrigger on sked__Job__c (after insert, after update) {
	if (trigger.isAfter) {
		if (trigger.isInsert) {
			skedJobHandler.onAfterInsert(trigger.new);
		}
		else if (trigger.isUpdate) {
			skedJobHandler.onAfterUpdate(trigger.new, trigger.oldMap);
		}
	}
}